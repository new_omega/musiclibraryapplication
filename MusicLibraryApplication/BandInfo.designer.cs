﻿namespace MusicLibraryApplication
{
    partial class BandInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BandInfo));
            this.photoPictureBox = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.deletePhotoButton = new System.Windows.Forms.Button();
            this.bandNameTextBox = new System.Windows.Forms.TextBox();
            this.bandIDlabel = new System.Windows.Forms.Label();
            this.choosePhotoButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.bandMembersTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.bandYearTextBox = new System.Windows.Forms.TextBox();
            this.newGenreTextBox = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.bandGenreComboBox = new System.Windows.Forms.ComboBox();
            this.genreTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mLADataBaseDataSet = new MusicLibraryApplication.MLADataBaseDataSet();
            this.addNewGenreButton = new System.Windows.Forms.Button();
            this.albumAddNewButton = new System.Windows.Forms.Button();
            this.bandAlbumNameTextBox = new System.Windows.Forms.TextBox();
            this.bandAlbumYearTextBox = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.bandAlbumGenreComboBox = new System.Windows.Forms.ComboBox();
            this.albumsDGV = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.albumTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.albumEditButton = new System.Windows.Forms.Button();
            this.albumDeleteDataButton = new System.Windows.Forms.Button();
            this.bandSaveDataButton = new System.Windows.Forms.Button();
            this.bandInfoTextBox = new System.Windows.Forms.TextBox();
            this.downloadInfoButton = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.bandDeleteDataButton = new System.Windows.Forms.Button();
            this.albumTableTableAdapter = new MusicLibraryApplication.MLADataBaseDataSetTableAdapters.AlbumTableTableAdapter();
            this.tableAdapterManager = new MusicLibraryApplication.MLADataBaseDataSetTableAdapters.TableAdapterManager();
            this.bandTableTableAdapter = new MusicLibraryApplication.MLADataBaseDataSetTableAdapters.BandTableTableAdapter();
            this.genreTableTableAdapter = new MusicLibraryApplication.MLADataBaseDataSetTableAdapters.GenreTableTableAdapter();
            this.bandTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.songBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.songTableTableAdapter = new MusicLibraryApplication.MLADataBaseDataSetTableAdapters.SongTableTableAdapter();
            this.closeFormButton = new System.Windows.Forms.Button();
            this.bandToolTip = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.photoPictureBox)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.genreTableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mLADataBaseDataSet)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.albumsDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.albumTableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandTableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.songBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // photoPictureBox
            // 
            this.photoPictureBox.Location = new System.Drawing.Point(9, 45);
            this.photoPictureBox.Name = "photoPictureBox";
            this.photoPictureBox.Size = new System.Drawing.Size(250, 250);
            this.photoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.photoPictureBox.TabIndex = 1;
            this.photoPictureBox.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Группа:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.deletePhotoButton);
            this.groupBox1.Controls.Add(this.bandNameTextBox);
            this.groupBox1.Controls.Add(this.bandIDlabel);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.choosePhotoButton);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.photoPictureBox);
            this.groupBox1.Location = new System.Drawing.Point(7, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(290, 328);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Информация о записи";
            // 
            // deletePhotoButton
            // 
            this.deletePhotoButton.Enabled = false;
            this.deletePhotoButton.Location = new System.Drawing.Point(130, 301);
            this.deletePhotoButton.Name = "deletePhotoButton";
            this.deletePhotoButton.Size = new System.Drawing.Size(129, 23);
            this.deletePhotoButton.TabIndex = 10;
            this.deletePhotoButton.Text = "Удалить фотографию";
            this.deletePhotoButton.UseVisualStyleBackColor = true;
            this.deletePhotoButton.Click += new System.EventHandler(this.deletePhotoButton_Click);
            // 
            // bandNameTextBox
            // 
            this.bandNameTextBox.Location = new System.Drawing.Point(57, 19);
            this.bandNameTextBox.Name = "bandNameTextBox";
            this.bandNameTextBox.Size = new System.Drawing.Size(160, 20);
            this.bandNameTextBox.TabIndex = 9;
            // 
            // bandIDlabel
            // 
            this.bandIDlabel.AutoSize = true;
            this.bandIDlabel.Location = new System.Drawing.Point(250, 22);
            this.bandIDlabel.Name = "bandIDlabel";
            this.bandIDlabel.Size = new System.Drawing.Size(42, 13);
            this.bandIDlabel.TabIndex = 8;
            this.bandIDlabel.Text = "id_here";
            // 
            // choosePhotoButton
            // 
            this.choosePhotoButton.Location = new System.Drawing.Point(9, 301);
            this.choosePhotoButton.Name = "choosePhotoButton";
            this.choosePhotoButton.Size = new System.Drawing.Size(75, 23);
            this.choosePhotoButton.TabIndex = 4;
            this.choosePhotoButton.Text = "Обзор...";
            this.choosePhotoButton.UseVisualStyleBackColor = true;
            this.choosePhotoButton.Click += new System.EventHandler(this.chooseCoverTextBox_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(223, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "ID:";
            // 
            // bandMembersTextBox
            // 
            this.bandMembersTextBox.Location = new System.Drawing.Point(95, 19);
            this.bandMembersTextBox.Multiline = true;
            this.bandMembersTextBox.Name = "bandMembersTextBox";
            this.bandMembersTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.bandMembersTextBox.Size = new System.Drawing.Size(186, 97);
            this.bandMembersTextBox.TabIndex = 5;
            this.bandToolTip.SetToolTip(this.bandMembersTextBox, "Моно указать, кто на чем играл и когда");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Состав:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 148);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Жанр:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 122);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Год создания:";
            // 
            // bandYearTextBox
            // 
            this.bandYearTextBox.Location = new System.Drawing.Point(95, 119);
            this.bandYearTextBox.Name = "bandYearTextBox";
            this.bandYearTextBox.Size = new System.Drawing.Size(186, 20);
            this.bandYearTextBox.TabIndex = 13;
            this.bandToolTip.SetToolTip(this.bandYearTextBox, "Год образования исполнителя или группы");
            this.bandYearTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.yearTextBox_KeyPress);
            // 
            // newGenreTextBox
            // 
            this.newGenreTextBox.Location = new System.Drawing.Point(116, 172);
            this.newGenreTextBox.Name = "newGenreTextBox";
            this.newGenreTextBox.Size = new System.Drawing.Size(165, 20);
            this.newGenreTextBox.TabIndex = 14;
            this.bandToolTip.SetToolTip(this.newGenreTextBox, "Добавьте новый жанр, если не можете найти нужный");
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.bandGenreComboBox);
            this.groupBox2.Controls.Add(this.addNewGenreButton);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.bandMembersTextBox);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.bandYearTextBox);
            this.groupBox2.Controls.Add(this.newGenreTextBox);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(7, 396);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(290, 206);
            this.groupBox2.TabIndex = 16;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Информация о группе";
            // 
            // bandGenreComboBox
            // 
            this.bandGenreComboBox.DataSource = this.genreTableBindingSource;
            this.bandGenreComboBox.DisplayMember = "GenreName";
            this.bandGenreComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.bandGenreComboBox.FormattingEnabled = true;
            this.bandGenreComboBox.Location = new System.Drawing.Point(95, 145);
            this.bandGenreComboBox.Name = "bandGenreComboBox";
            this.bandGenreComboBox.Size = new System.Drawing.Size(186, 21);
            this.bandGenreComboBox.TabIndex = 29;
            this.bandGenreComboBox.ValueMember = "ID";
            // 
            // genreTableBindingSource
            // 
            this.genreTableBindingSource.DataMember = "GenreTable";
            this.genreTableBindingSource.DataSource = this.mLADataBaseDataSet;
            // 
            // mLADataBaseDataSet
            // 
            this.mLADataBaseDataSet.DataSetName = "MLADataBaseDataSet";
            this.mLADataBaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // addNewGenreButton
            // 
            this.addNewGenreButton.Location = new System.Drawing.Point(5, 169);
            this.addNewGenreButton.Name = "addNewGenreButton";
            this.addNewGenreButton.Size = new System.Drawing.Size(104, 25);
            this.addNewGenreButton.TabIndex = 29;
            this.addNewGenreButton.Text = "Добавить новый";
            this.addNewGenreButton.UseVisualStyleBackColor = true;
            this.addNewGenreButton.Click += new System.EventHandler(this.addNewGenreButton_Click);
            // 
            // albumAddNewButton
            // 
            this.albumAddNewButton.Location = new System.Drawing.Point(6, 254);
            this.albumAddNewButton.Name = "albumAddNewButton";
            this.albumAddNewButton.Size = new System.Drawing.Size(75, 23);
            this.albumAddNewButton.TabIndex = 19;
            this.albumAddNewButton.Text = "Добавить";
            this.albumAddNewButton.UseVisualStyleBackColor = true;
            this.albumAddNewButton.Click += new System.EventHandler(this.addAlbumButton_Click);
            // 
            // bandAlbumNameTextBox
            // 
            this.bandAlbumNameTextBox.Location = new System.Drawing.Point(6, 283);
            this.bandAlbumNameTextBox.Name = "bandAlbumNameTextBox";
            this.bandAlbumNameTextBox.Size = new System.Drawing.Size(273, 20);
            this.bandAlbumNameTextBox.TabIndex = 20;
            this.bandAlbumNameTextBox.Text = "Введите название альбома";
            this.bandToolTip.SetToolTip(this.bandAlbumNameTextBox, "Название альбома группы");
            this.bandAlbumNameTextBox.Click += new System.EventHandler(this.nameOfAlbumTextBox_TextChanged);
            this.bandAlbumNameTextBox.TextChanged += new System.EventHandler(this.nameOfAlbumTextBox_TextChanged);
            // 
            // bandAlbumYearTextBox
            // 
            this.bandAlbumYearTextBox.Location = new System.Drawing.Point(285, 283);
            this.bandAlbumYearTextBox.Name = "bandAlbumYearTextBox";
            this.bandAlbumYearTextBox.Size = new System.Drawing.Size(47, 20);
            this.bandAlbumYearTextBox.TabIndex = 21;
            this.bandAlbumYearTextBox.Text = "Год";
            this.bandToolTip.SetToolTip(this.bandAlbumYearTextBox, "Год выхода альбома группы");
            this.bandAlbumYearTextBox.Click += new System.EventHandler(this.yearOfAlbumTextBox_TextChanged);
            this.bandAlbumYearTextBox.TextChanged += new System.EventHandler(this.yearOfAlbumTextBox_TextChanged);
            this.bandAlbumYearTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.yearOfAlbumTextBox_KeyPress);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.bandAlbumGenreComboBox);
            this.groupBox3.Controls.Add(this.albumsDGV);
            this.groupBox3.Controls.Add(this.albumEditButton);
            this.groupBox3.Controls.Add(this.albumDeleteDataButton);
            this.groupBox3.Controls.Add(this.albumAddNewButton);
            this.groupBox3.Controls.Add(this.bandAlbumYearTextBox);
            this.groupBox3.Controls.Add(this.bandAlbumNameTextBox);
            this.groupBox3.Location = new System.Drawing.Point(303, 354);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(455, 316);
            this.groupBox3.TabIndex = 23;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Список альбомов";
            // 
            // bandAlbumGenreComboBox
            // 
            this.bandAlbumGenreComboBox.DataSource = this.genreTableBindingSource;
            this.bandAlbumGenreComboBox.DisplayMember = "GenreName";
            this.bandAlbumGenreComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.bandAlbumGenreComboBox.FormattingEnabled = true;
            this.bandAlbumGenreComboBox.Location = new System.Drawing.Point(168, 254);
            this.bandAlbumGenreComboBox.Name = "bandAlbumGenreComboBox";
            this.bandAlbumGenreComboBox.Size = new System.Drawing.Size(164, 21);
            this.bandAlbumGenreComboBox.TabIndex = 30;
            this.bandAlbumGenreComboBox.ValueMember = "ID";
            // 
            // albumsDGV
            // 
            this.albumsDGV.AllowUserToAddRows = false;
            this.albumsDGV.AllowUserToDeleteRows = false;
            this.albumsDGV.AutoGenerateColumns = false;
            this.albumsDGV.BackgroundColor = System.Drawing.Color.DarkCyan;
            this.albumsDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.albumsDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15});
            this.albumsDGV.DataSource = this.albumTableBindingSource;
            this.albumsDGV.Location = new System.Drawing.Point(6, 19);
            this.albumsDGV.Name = "albumsDGV";
            this.albumsDGV.ReadOnly = true;
            this.albumsDGV.RowHeadersVisible = false;
            this.albumsDGV.Size = new System.Drawing.Size(445, 229);
            this.albumsDGV.TabIndex = 28;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "ID";
            this.dataGridViewTextBoxColumn8.HeaderText = "ID";
            this.dataGridViewTextBoxColumn8.MinimumWidth = 25;
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 25;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "BID";
            this.dataGridViewTextBoxColumn9.HeaderText = "BID";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Visible = false;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "GID";
            this.dataGridViewTextBoxColumn10.HeaderText = "GID";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Visible = false;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn11.DataPropertyName = "AlbumName";
            this.dataGridViewTextBoxColumn11.HeaderText = "Альбом";
            this.dataGridViewTextBoxColumn11.MinimumWidth = 160;
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "AlbumYear";
            this.dataGridViewTextBoxColumn12.HeaderText = "Год";
            this.dataGridViewTextBoxColumn12.MinimumWidth = 55;
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.Width = 55;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "AlbumGenre";
            this.dataGridViewTextBoxColumn13.HeaderText = "Жанр";
            this.dataGridViewTextBoxColumn13.MinimumWidth = 120;
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.Width = 120;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "AlbumLabel";
            this.dataGridViewTextBoxColumn14.HeaderText = "AlbumLabel";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.Visible = false;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "AlbumArtist";
            this.dataGridViewTextBoxColumn15.HeaderText = "AlbumArtist";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.Visible = false;
            // 
            // albumTableBindingSource
            // 
            this.albumTableBindingSource.DataMember = "AlbumTable";
            this.albumTableBindingSource.DataSource = this.mLADataBaseDataSet;
            // 
            // albumEditButton
            // 
            this.albumEditButton.Location = new System.Drawing.Point(338, 254);
            this.albumEditButton.Name = "albumEditButton";
            this.albumEditButton.Size = new System.Drawing.Size(113, 49);
            this.albumEditButton.TabIndex = 28;
            this.albumEditButton.Text = "Изменить информацию об альбоме";
            this.bandToolTip.SetToolTip(this.albumEditButton, "Переход к редактору альбомов");
            this.albumEditButton.UseVisualStyleBackColor = true;
            this.albumEditButton.Click += new System.EventHandler(this.editAlbumInfo_Click);
            // 
            // albumDeleteDataButton
            // 
            this.albumDeleteDataButton.Enabled = false;
            this.albumDeleteDataButton.Location = new System.Drawing.Point(87, 254);
            this.albumDeleteDataButton.Name = "albumDeleteDataButton";
            this.albumDeleteDataButton.Size = new System.Drawing.Size(75, 23);
            this.albumDeleteDataButton.TabIndex = 23;
            this.albumDeleteDataButton.Text = "Удалить";
            this.albumDeleteDataButton.UseVisualStyleBackColor = true;
            this.albumDeleteDataButton.Click += new System.EventHandler(this.deleteAlbumButton_Click);
            // 
            // bandSaveDataButton
            // 
            this.bandSaveDataButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bandSaveDataButton.Location = new System.Drawing.Point(7, 608);
            this.bandSaveDataButton.Name = "bandSaveDataButton";
            this.bandSaveDataButton.Size = new System.Drawing.Size(77, 37);
            this.bandSaveDataButton.TabIndex = 24;
            this.bandSaveDataButton.Text = "Сохранить запись";
            this.bandSaveDataButton.UseVisualStyleBackColor = true;
            this.bandSaveDataButton.Click += new System.EventHandler(this.saveEntryButton_Click);
            // 
            // bandInfoTextBox
            // 
            this.bandInfoTextBox.Location = new System.Drawing.Point(303, 34);
            this.bandInfoTextBox.Multiline = true;
            this.bandInfoTextBox.Name = "bandInfoTextBox";
            this.bandInfoTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.bandInfoTextBox.Size = new System.Drawing.Size(455, 314);
            this.bandInfoTextBox.TabIndex = 25;
            // 
            // downloadInfoButton
            // 
            this.downloadInfoButton.Location = new System.Drawing.Point(7, 346);
            this.downloadInfoButton.Name = "downloadInfoButton";
            this.downloadInfoButton.Size = new System.Drawing.Size(160, 42);
            this.downloadInfoButton.TabIndex = 26;
            this.downloadInfoButton.Text = "Загрузить изображение и информацию из интернета";
            this.bandToolTip.SetToolTip(this.downloadInfoButton, "Нажмите, чтобы загрузить обложку и описание группы с сайта LastFM");
            this.downloadInfoButton.UseVisualStyleBackColor = true;
            this.downloadInfoButton.Click += new System.EventHandler(this.downloadInfoButton_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(303, 12);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 13);
            this.label6.TabIndex = 27;
            this.label6.Text = "Краткая биография:";
            // 
            // bandDeleteDataButton
            // 
            this.bandDeleteDataButton.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.bandDeleteDataButton.Enabled = false;
            this.bandDeleteDataButton.Location = new System.Drawing.Point(90, 608);
            this.bandDeleteDataButton.Name = "bandDeleteDataButton";
            this.bandDeleteDataButton.Size = new System.Drawing.Size(77, 37);
            this.bandDeleteDataButton.TabIndex = 28;
            this.bandDeleteDataButton.Text = "Удалить запись";
            this.bandDeleteDataButton.UseVisualStyleBackColor = true;
            this.bandDeleteDataButton.Click += new System.EventHandler(this.deleteBandButton_Click);
            // 
            // albumTableTableAdapter
            // 
            this.albumTableTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.AlbumTableTableAdapter = this.albumTableTableAdapter;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BandTableTableAdapter = this.bandTableTableAdapter;
            this.tableAdapterManager.GenreTableTableAdapter = this.genreTableTableAdapter;
            this.tableAdapterManager.SongTableTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = MusicLibraryApplication.MLADataBaseDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // bandTableTableAdapter
            // 
            this.bandTableTableAdapter.ClearBeforeFill = true;
            // 
            // genreTableTableAdapter
            // 
            this.genreTableTableAdapter.ClearBeforeFill = true;
            // 
            // bandTableBindingSource
            // 
            this.bandTableBindingSource.DataMember = "BandTable";
            this.bandTableBindingSource.DataSource = this.mLADataBaseDataSet;
            // 
            // songBindingSource
            // 
            this.songBindingSource.DataMember = "SongTable";
            this.songBindingSource.DataSource = this.mLADataBaseDataSet;
            // 
            // songTableTableAdapter
            // 
            this.songTableTableAdapter.ClearBeforeFill = true;
            // 
            // closeFormButton
            // 
            this.closeFormButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.closeFormButton.Location = new System.Drawing.Point(213, 608);
            this.closeFormButton.Name = "closeFormButton";
            this.closeFormButton.Size = new System.Drawing.Size(75, 37);
            this.closeFormButton.TabIndex = 33;
            this.closeFormButton.Text = "Закрыть";
            this.bandToolTip.SetToolTip(this.closeFormButton, "Закрыть редактор без сохранения изменений");
            this.closeFormButton.UseVisualStyleBackColor = true;
            // 
            // BandInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.ClientSize = new System.Drawing.Size(770, 676);
            this.Controls.Add(this.closeFormButton);
            this.Controls.Add(this.bandDeleteDataButton);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.bandInfoTextBox);
            this.Controls.Add(this.downloadInfoButton);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.bandSaveDataButton);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BandInfo";
            this.Text = "Редактор групп";
            ((System.ComponentModel.ISupportInitialize)(this.photoPictureBox)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.genreTableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mLADataBaseDataSet)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.albumsDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.albumTableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandTableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.songBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox photoPictureBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button choosePhotoButton;
        private System.Windows.Forms.TextBox bandMembersTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox bandYearTextBox;
        private System.Windows.Forms.TextBox newGenreTextBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button albumAddNewButton;
        private System.Windows.Forms.TextBox bandAlbumNameTextBox;
        private System.Windows.Forms.TextBox bandAlbumYearTextBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button bandSaveDataButton;
        private System.Windows.Forms.TextBox bandNameTextBox;
        private System.Windows.Forms.Button albumDeleteDataButton;
        private System.Windows.Forms.TextBox bandInfoTextBox;
        private System.Windows.Forms.Button downloadInfoButton;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button addNewGenreButton;
        private System.Windows.Forms.Button albumEditButton;
        private System.Windows.Forms.Button bandDeleteDataButton;
        private MLADataBaseDataSet mLADataBaseDataSet;
        private System.Windows.Forms.BindingSource albumTableBindingSource;
        private MLADataBaseDataSetTableAdapters.AlbumTableTableAdapter albumTableTableAdapter;
        private MLADataBaseDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView albumsDGV;
        private MLADataBaseDataSetTableAdapters.GenreTableTableAdapter genreTableTableAdapter;
        private System.Windows.Forms.BindingSource genreTableBindingSource;
        private System.Windows.Forms.ComboBox bandGenreComboBox;
        private System.Windows.Forms.ComboBox bandAlbumGenreComboBox;
        private MLADataBaseDataSetTableAdapters.BandTableTableAdapter bandTableTableAdapter;
        private System.Windows.Forms.BindingSource bandTableBindingSource;
        private System.Windows.Forms.Button deletePhotoButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.BindingSource songBindingSource;
        private MLADataBaseDataSetTableAdapters.SongTableTableAdapter songTableTableAdapter;
        public System.Windows.Forms.Label bandIDlabel;
        private System.Windows.Forms.Button closeFormButton;
        private System.Windows.Forms.ToolTip bandToolTip;
    }
}