﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using System.Net;

namespace MusicLibraryApplication
{
    public partial class BandInfo : Form
    {
        /// <summary>
        /// Конструктор используется в случае создания нового исполнителя
        /// </summary>
        public BandInfo()
        {
            InitializeComponent();

            this.genreTableTableAdapter.Fill(this.mLADataBaseDataSet.GenreTable);
            this.bandTableTableAdapter.Fill(this.mLADataBaseDataSet.BandTable);
            this.genreTableBindingSource.Sort = "GenreName ASC";

            bandIDlabel.Text = (this.bandTableBindingSource.Count + 1).ToString();
            AlbumsInDBTotal = this.albumTableBindingSource.Count + 1;
            NewBand = true;
        }

        /// <summary>
        /// Конструктор используется в случае просмотра данных уже существующего исполнителя
        /// <param name="ID">Идентефикатор просматриваемой группы</param>
        public BandInfo(int ID, string name)
        {
            InitializeComponent();            

            // Заполняем все необходимые элементы данными из базы
            this.genreTableTableAdapter.Fill(this.mLADataBaseDataSet.GenreTable);
            this.albumTableTableAdapter.FillBy(this.mLADataBaseDataSet.AlbumTable, ID);
            this.bandTableTableAdapter.Fill(this.mLADataBaseDataSet.BandTable);
            this.genreTableBindingSource.Sort = "GenreName ASC";
            bandDeleteDataButton.Enabled = true;
            bandIDlabel.Text = ID.ToString();
            bandNameTextBox.Text = name;
            bandYearTextBox.Text = this.bandTableTableAdapter.GetYearByID(ID).ToString();
            bandGenreComboBox.Text = this.bandTableTableAdapter.GetGenreByID(ID);
            bandInfoTextBox.Text = this.bandTableTableAdapter.GetInfoByID(ID);
            bandMembersTextBox.Text = this.bandTableTableAdapter.GetMembersByID(ID);
            photoPictureBox.ImageLocation = this.bandTableTableAdapter.GetAvatarPathByID(ID);
            if (photoPictureBox.ImageLocation == null || photoPictureBox.ImageLocation == "") deletePhotoButton.Enabled = false;
            else deletePhotoButton.Enabled = true;

            if (!Options.ifAdmin)
            {
                bandSaveDataButton.Enabled = false;
                bandDeleteDataButton.Enabled = false;
                choosePhotoButton.Enabled = false;
                deletePhotoButton.Enabled = false;
                albumAddNewButton.Enabled = false;
                albumDeleteDataButton.Enabled = false;
                addNewGenreButton.Enabled = false;
                albumEditButton.Enabled = false;
            }

            NewBand = false;
        }

        // Так как AlbumTableTableAdapter привязан к одной группе, нам нужно где-то хранить общее количество альбомов в базе,
        // чтобы задавать ID новому альбому.
        public int AlbumsInDBTotal;
        public bool NewBand;

        private void downloadInfoButton_Click(object sender, EventArgs e)
        {
            string savedFileName;
            if (bandNameTextBox.Text.Length == 0)
            {
                MessageBox.Show("Введите название группы!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            string linkToAv;

            bandInfoTextBox.Text = Music.DownloadInfo(bandNameTextBox.Text, out linkToAv);
            if (bandInfoTextBox.Text == "error")
            {
                bandInfoTextBox.Text = "";
                return;
            }
 
            
            savedFileName = bandIDlabel.Text + "_" + bandNameTextBox.Text + ".jpg";
            WebClient client = new WebClient();
            client.DownloadFile(linkToAv, savedFileName);
            photoPictureBox.ImageLocation = savedFileName;
        }

        private void saveEntryButton_Click(object sender, EventArgs e)
        {
            if (bandNameTextBox.Text == "" || bandYearTextBox.Text == "" || bandGenreComboBox.Text == "")
            {
                MessageBox.Show("Для создания группы необходимо задать\n\"Имя\", \"Год создания\" и \"Жанр\"!",
                    "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            string name = bandNameTextBox.Text,
                genre = bandGenreComboBox.Text,
                members = bandMembersTextBox.Text,
                avatar = photoPictureBox.ImageLocation,
                bio = bandInfoTextBox.Text;
            int ID = int.Parse(bandIDlabel.Text), 
                gID = this.genreTableBindingSource.Find("GenreName", genre)+1,
                year = int.Parse(bandYearTextBox.Text),
                albumsInDB = albumsDGV.RowCount;

            if (NewBand)
            {
                this.bandTableTableAdapter.Insert(ID, gID, name, year, genre, albumsInDB,avatar,members,bio);
                this.bandTableTableAdapter.Update(this.mLADataBaseDataSet.BandTable);
            }
            else
            {
                this.bandTableTableAdapter.UpdateAllByID(gID, name, year, genre, albumsInDB,avatar,members,bio, ID);
                this.bandTableTableAdapter.Update(this.mLADataBaseDataSet.BandTable);
            }
        }

        private bool saveEntryButton_Click()
        {
            if (bandNameTextBox.Text == "" || bandYearTextBox.Text == "" || bandGenreComboBox.Text == "")
            {
                MessageBox.Show("Для создания группы необходимо задать\n\"Имя\", \"Год создания\" и \"Жанр\"!",
                    "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            string name = bandNameTextBox.Text,
                genre = bandGenreComboBox.Text,
                members = bandMembersTextBox.Text,
                avatar = photoPictureBox.ImageLocation,
                bio = bandInfoTextBox.Text;
            int ID = int.Parse(bandIDlabel.Text),
                gID = this.genreTableBindingSource.Find("GenreName", genre) + 1,
                year = int.Parse(bandYearTextBox.Text),
                albumsInDB = albumsDGV.RowCount;

            if (NewBand)
            {
                this.bandTableTableAdapter.Insert(ID, gID, name, year, genre, albumsInDB, avatar, members, bio);
                this.bandTableTableAdapter.Update(this.mLADataBaseDataSet.BandTable);
            }
            else
            {
                this.bandTableTableAdapter.UpdateAllByID(gID, name, year, genre, albumsInDB, avatar,members, bio, ID);
                this.bandTableTableAdapter.Update(this.mLADataBaseDataSet.BandTable);
            }
            bandDeleteDataButton.Enabled = true;
            return true;
        }    

        private void yearTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (bandYearTextBox.Text.Length == 4 && e.KeyChar != 8) e.Handled = true;
            else if (bandYearTextBox.Text.Length == 0 && (e.KeyChar != 8 && (e.KeyChar < 48 || e.KeyChar > 50)))
                e.Handled = true;
            else if (e.KeyChar != 8 && (e.KeyChar < 48 || e.KeyChar > 57))
                e.Handled = true;
        }

        private void yearOfAlbumTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (bandAlbumYearTextBox.Text.Length == 4 && e.KeyChar != 8) e.Handled = true;
            else if (bandAlbumYearTextBox.Text.Length == 0 && (e.KeyChar != 8 && (e.KeyChar < 48 || e.KeyChar > 50)))
                e.Handled = true;
            else if (e.KeyChar != 8 && (e.KeyChar < 48 || e.KeyChar > 57))
                e.Handled = true;
        }

        private void chooseCoverTextBox_Click(object sender, EventArgs e)
        {
            OpenFileDialog opf = new OpenFileDialog();
            string path, newFileName;

            opf.Filter = "Изображения (*.jpg, *.png, *.bmp)|*.jpg;*.png;*.bmp";
            if (opf.ShowDialog() == DialogResult.Cancel) return;
            path = opf.FileName;
            

            FileStream inputFile;
            FileStream outputFile;

            inputFile = new FileStream(path, FileMode.Open);
            newFileName = bandIDlabel.Text + "_" + bandNameTextBox.Text + path.Substring(path.Length - 4);

            //Теперь надо создать копию
            outputFile = new FileStream(newFileName, FileMode.Create);

            //Теперь копирование
            int i;
            do
            {
                i = inputFile.ReadByte();
                if (i != -1)
                {
                    outputFile.WriteByte((byte)i);
                }
            }
            while (i != -1);

            inputFile.Close();
            outputFile.Close();

            photoPictureBox.ImageLocation = path;
            deletePhotoButton.Enabled = true;
        }

        private void nameOfAlbumTextBox_TextChanged(object sender, EventArgs e)
        {
            if (bandAlbumNameTextBox.Text == "Введите название альбома") bandAlbumNameTextBox.Text = "";
        }

        private void yearOfAlbumTextBox_TextChanged(object sender, EventArgs e)
        {
            if (bandAlbumYearTextBox.Text == "Год") bandAlbumYearTextBox.Text = "";
        }

        private void addAlbumButton_Click(object sender, EventArgs e)
        {
            if ((bandAlbumNameTextBox.Text == "" || bandAlbumNameTextBox.Text == "Введите название альбома") || 
                (bandAlbumYearTextBox.Text == "" || bandAlbumYearTextBox.Text == "Год") || bandAlbumGenreComboBox.Text == "")
            {
                MessageBox.Show("Для создания альбома необходимо задать\nимя, год создания и жанр!",
                    "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            string name, genre;
            int id, bid, gid, year;
            AlbumsInDBTotal++;
            name = bandAlbumNameTextBox.Text;
            genre = bandAlbumGenreComboBox.Text;
            year = int.Parse(bandAlbumYearTextBox.Text);
            id = AlbumsInDBTotal;
            bid = int.Parse(bandIDlabel.Text);
            gid = this.genreTableBindingSource.Find("GenreName", genre)+1;

            if (this.bandTableBindingSource.Find("ID", bid) == -1)
            {
                if (MessageBox.Show("Вы ещё не добавили текущую группу в базу.\nПрежде чем добавить альбомы, " +
                    "вы должны добавить эту группу в базу.\nНажмите \"ОК\", чтобы добавить группу в базу и" +
                    " добавить альбом к текущей группе.",
                    "Внимание!", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) == DialogResult.Cancel) return;
                else
                {
                    if (saveEntryButton_Click()) gid = this.genreTableBindingSource.Find("GenreName", genre) + 1;
                    else return;
                }            
            }

            this.albumTableTableAdapter.Insert(id, bid, gid, name, year, genre, "", bandNameTextBox.Text,"","");
            this.albumTableTableAdapter.Update(this.mLADataBaseDataSet.AlbumTable);
            this.albumTableTableAdapter.FillBy(this.mLADataBaseDataSet.AlbumTable, bid);           

            bandAlbumNameTextBox.Text = "Введите название альбома";
            bandAlbumYearTextBox.Text = "Год";
            if (NewBand) NewBand = false;
        }

        private void addNewGenreButton_Click(object sender, EventArgs e)
        {
            if (newGenreTextBox.Text == "")
            {
                MessageBox.Show("Введите название жанра!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            string name = newGenreTextBox.Text;
            int id;
            id = this.mLADataBaseDataSet.GenreTable.Rows.Count + 1;
            this.genreTableTableAdapter.Insert(id, name, 0);
            this.genreTableTableAdapter.Update(this.mLADataBaseDataSet.GenreTable);
            this.genreTableTableAdapter.Fill(this.mLADataBaseDataSet.GenreTable);

            newGenreTextBox.Text = "";
        }

        private void deleteAlbumButton_Click(object sender, EventArgs e)
        {
            this.songTableTableAdapter.DeleteByID(int.Parse(albumsDGV.CurrentRow.Cells[0].Value.ToString()));
            this.songTableTableAdapter.Update(this.mLADataBaseDataSet.SongTable);
            this.albumTableTableAdapter.DeleteByID(int.Parse(albumsDGV.CurrentRow.Cells[0].Value.ToString()));
            this.albumTableTableAdapter.FillBy(this.mLADataBaseDataSet.AlbumTable, int.Parse(bandIDlabel.Text));
        }

        private void deleteBandButton_Click(object sender, EventArgs e)
        {
            if (NewBand) bandDeleteDataButton.DialogResult = DialogResult.Cancel;
        }

        private void deletePhotoButton_Click(object sender, EventArgs e)
        {
            System.IO.File.Delete(photoPictureBox.ImageLocation);
            photoPictureBox.ImageLocation = null;
        }

        private void editAlbumInfo_Click(object sender, EventArgs e)
        {
            string albumName;
            int aID;

            try
            {
                albumName = albumsDGV.CurrentRow.Cells[3].Value.ToString();
            }
            catch
            {
                MessageBox.Show("Сначала выберете альбом, у которого хотите просмотреть/изменить информацию!",
                    "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            aID = int.Parse(albumsDGV.CurrentRow.Cells[0].Value.ToString());

            AlbumInfo form = new AlbumInfo(aID, albumName);
            switch (form.ShowDialog())
            {
                case DialogResult.Abort:
                    {
                        if (this.albumTableBindingSource.Count == 0)
                        {
                            MessageBox.Show("База альбомов пуста!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            albumAddNewButton.Enabled = false;
                            return;
                        }

                        int id = int.Parse(albumsDGV.CurrentRow.Cells[0].Value.ToString()),
                            albumsCount = (int)

                        this.bandTableTableAdapter.UpdateAlbumsCount((int)this.bandTableTableAdapter.GetAlbumsCountByID(int.Parse(albumsDGV.CurrentRow.Cells[1].Value.ToString())) - 1, int.Parse(albumsDGV.CurrentRow.Cells[1].Value.ToString()));
                        this.bandTableTableAdapter.Update(this.mLADataBaseDataSet.BandTable);
                        this.albumTableTableAdapter.DeleteSongsById(id);
                        this.albumTableTableAdapter.DeleteByID(id);
                        this.albumTableTableAdapter.Update(mLADataBaseDataSet.AlbumTable);

                        this.albumTableTableAdapter.Fill(this.mLADataBaseDataSet.AlbumTable);
                        this.bandTableTableAdapter.Fill(this.mLADataBaseDataSet.BandTable);
                        return;
                    }
                case DialogResult.Cancel:
                    {
                        return;
                    }
                case DialogResult.OK:
                    {
                        this.albumTableTableAdapter.Fill(this.mLADataBaseDataSet.AlbumTable);
                        this.bandTableTableAdapter.Fill(this.mLADataBaseDataSet.BandTable);
                        break;
                    }
            } 
        }
    }
}
