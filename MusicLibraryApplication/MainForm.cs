﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;

namespace MusicLibraryApplication
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                FillAllTables();

                bandDeleteDataButton.Enabled = this.bandTableBindingSource.Count != 0 ? true : false;
                albumDeleteDataButton.Enabled = this.albumTableBindingSource.Count != 0 ? true : false;
                albumAddNewButton.Enabled = this.bandTableBindingSource.Count != 0 ? true : false;
                genreDeleteDataButton.Enabled = this.genreTableBindingSource.Count != 0 ? true : false;
                genreChangeDataButton.Enabled = this.genreTableBindingSource.Count != 0 ? true : false;
                genreNameTextBox.Text = this.genreTableTableAdapter.GetNameByID(1);
                genreYearTextBox.Text = this.genreTableTableAdapter.GetYearByID(1).ToString();
                this.genreTableBindingSource.Sort = "GenreName ASC";

                if (Options.ifAdmin == false)
                {
                    addToolStripMenuItem.Enabled = false;
                    bandAddNewButton.Enabled = false;
                    bandDeleteDataButton.Enabled = false;
                    albumAddNewButton.Enabled = false;
                    albumDeleteDataButton.Enabled = false;
                    genreAddNewButton.Enabled = false;
                    genreChangeDataButton.Enabled = false;
                    genreDeleteDataButton.Enabled = false;
                }
            }

            catch
            {
                MessageBox.Show("Не удалось обнаружить подключение к серверу/базу данных. Для устранения этой проблемы свяжитесь с разработчиком."+
                    "\nПрограмма будет закрыта.", "Critical Error appeared", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void FillAllTables()
        {
            try
            {
                this.songTableTableAdapter.Fill(this.mLADataBaseDataSet.SongTable);
                this.bandTableTableAdapter.Fill(this.mLADataBaseDataSet.BandTable);
                this.albumTableTableAdapter.Fill(this.mLADataBaseDataSet.AlbumTable);
                this.genreTableTableAdapter.Fill(this.mLADataBaseDataSet.GenreTable);
            }
            catch (Exception ex)
            {

            }
        }

    #region Методы, проверяющие правильность ввода
        private void bandYearTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (bandYearTextBox.Text.Length == 4 && e.KeyChar != 8) e.Handled = true;
            else if (bandYearTextBox.Text.Length == 0 && (e.KeyChar != 8 && (e.KeyChar < 48 || e.KeyChar > 50)))
                e.Handled = true;
            else if (e.KeyChar != 8 && (e.KeyChar < 48 || e.KeyChar > 57))
                e.Handled = true;
        }

        private void bandFirstYearTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (bandFirstYearTextBox.Text.Length == 4 && e.KeyChar != 8) e.Handled = true;
            else if (bandFirstYearTextBox.Text.Length == 0 && (e.KeyChar != 8 && (e.KeyChar < 48 || e.KeyChar > 50)))
                e.Handled = true;
            else if (e.KeyChar != 8 && (e.KeyChar < 48 || e.KeyChar > 57))
                e.Handled = true;
        }

        private void bandLastYearTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (bandLastYearTextBox.Text.Length == 4 && e.KeyChar != 8) e.Handled = true;
            else if (bandLastYearTextBox.Text.Length == 0 && (e.KeyChar != 8 && (e.KeyChar < 48 || e.KeyChar > 50)))
                e.Handled = true;
            else if (e.KeyChar != 8 && (e.KeyChar < 48 || e.KeyChar > 57))
                e.Handled = true;
        }

        private void addNewGenreTextBox_TextChanged(object sender, EventArgs e)
        {
            if (albumAddNewGenreTextBox.Text != "") albumAddNewGenreButton.Enabled = true;
            else albumAddNewGenreButton.Enabled = false;
        }

        public void ValidateBandSearchButton()
        {
            if ((bandSearchNameTextBox.Text != "" && bandSearchNameCheckBox.Checked == true) ||
                ((bandFirstYearTextBox.Text != "" || bandLastYearTextBox.Text != "") && bandSearchYearCheckBox.Checked == true) ||
                (bandSearchGenreCheckBox.Checked == true)) bandSearchButton.Enabled = true;
            else bandSearchButton.Enabled = false;
        }

        public void ValidateAlbumSearchButton()
        {
            if ((albumSearchNameTextBox.Text != "" && albumSearchNameCheckBox.Checked == true) ||
                (albumFirstYearTextBox.Text != "" || albumLastYearTextBox.Text != "") && albumSearchYearCheckBox.Checked == true ||
                (albumSearchBandComboBox.Text != "" && albumSearchArtistCheckBox.Checked == true) ||
                albumSearchGenreCheckBox.Checked == true) albumSearchButton.Enabled = true;
            else albumSearchButton.Enabled = false;
        }

        public void SearchTextChanged(object sender, EventArgs e)
        {
            ValidateBandSearchButton();
        }

        private void albumSearchTextChanged(object sender, EventArgs e)
        {
            ValidateAlbumSearchButton();
        }

        private void albumSearchNameCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (albumSearchNameCheckBox.Checked == true) albumSearchNameTextBox.Enabled = true;
            else albumSearchNameTextBox.Enabled = false;
            ValidateAlbumSearchButton();
        }

        private void albumSearchArtistCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (albumSearchArtistCheckBox.Checked == true) albumSearchBandComboBox.Enabled = true;
            else albumSearchBandComboBox.Enabled = false;
                
            ValidateAlbumSearchButton();
        }

        private void albumSearchYearCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (albumSearchYearCheckBox.Checked == true)
            {
                albumFirstYearTextBox.Enabled = true;
                albumLastYearTextBox.Enabled = true;
                albumLastYearTextBox.Text = "";
            }
            else
            {
                albumFirstYearTextBox.Enabled = false;
                albumLastYearTextBox.Enabled = false;
                albumLastYearTextBox.Text = "";
                albumFirstYearTextBox.Text = "";
            }
            ValidateAlbumSearchButton();
        }

        private void albumSearchGenreCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (albumSearchGenreCheckBox.Checked == true) albumSearchGenreComboBox.Enabled = true;
            else albumSearchGenreComboBox.Enabled = false;
            ValidateAlbumSearchButton();
        }

        private void bandNameCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (bandSearchNameCheckBox.Checked == true) bandSearchNameTextBox.Enabled = true;
            else
            {
                bandSearchNameTextBox.Enabled = false;
                bandSearchNameTextBox.Text = "";
            }
            ValidateBandSearchButton();
        }

        private void bandYearCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (bandSearchYearCheckBox.Checked == true)
            {
                bandFirstYearTextBox.Enabled = true;
                bandLastYearTextBox.Enabled = true;
                bandLastYearTextBox.Text = "";
            }
            else
            {
                bandFirstYearTextBox.Enabled = false;
                bandLastYearTextBox.Enabled = false;
                bandLastYearTextBox.Text = "";
                bandFirstYearTextBox.Text = "";
            }
            ValidateBandSearchButton();
        }

        private void bandGenreCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (bandSearchGenreCheckBox.Checked == true) bandSearchGenreComboBox.Enabled = true;
            else bandSearchGenreComboBox.Enabled = false;
            ValidateBandSearchButton();
        }

        private void newGenreTextBox_TextChanged(object sender, EventArgs e)
        {
            if (bandNewGenreTextBox.Text.Length > 0) bandAddNewGenreButton.Enabled = true;
            else bandAddNewGenreButton.Enabled = false;
        }

        private void bandSearchClearButton_Click(object sender, EventArgs e)
        {
            this.bandTableTableAdapter.Fill(this.mLADataBaseDataSet.BandTable);
            bandSearchClearButton.Enabled = false;
            saveBandSearchResults.Enabled = false;
        }

        private void bandSearchNameTextBox_Click(object sender, EventArgs e)
        {
            bandSearchNameTextBox.SelectionStart = 0;
            bandSearchNameTextBox.SelectionLength = bandSearchNameTextBox.Text.Length;
        }

        private void bandFirstYearTextBox_Click(object sender, EventArgs e)
        {
            bandFirstYearTextBox.SelectionStart = 0;
            bandFirstYearTextBox.SelectionLength = bandFirstYearTextBox.Text.Length;
        }

        private void bandLastYearTextBox_Click(object sender, EventArgs e)
        {
            bandLastYearTextBox.SelectionStart = 0;
            bandLastYearTextBox.SelectionLength = bandLastYearTextBox.Text.Length;
        }

        private void albumFirstYear_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (albumFirstYearTextBox.Text.Length == 4 && e.KeyChar != 8) e.Handled = true;
            else if (albumFirstYearTextBox.Text.Length == 0 && (e.KeyChar != 8 && (e.KeyChar < 48 || e.KeyChar > 50)))
                e.Handled = true;
            else if (e.KeyChar != 8 && (e.KeyChar < 48 || e.KeyChar > 57))
                e.Handled = true;
        }

        private void albumLastYear_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (albumLastYearTextBox.Text.Length == 4 && e.KeyChar != 8) e.Handled = true;
            else if (albumLastYearTextBox.Text.Length == 0 && (e.KeyChar != 8 && (e.KeyChar < 48 || e.KeyChar > 50)))
                e.Handled = true;
            else if (e.KeyChar != 8 && (e.KeyChar < 48 || e.KeyChar > 57))
                e.Handled = true;
        }

        private void albumYearTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (albumYearTextBox.Text.Length == 4 && e.KeyChar != 8) e.Handled = true;
            else if (albumYearTextBox.Text.Length == 0 && (e.KeyChar != 8 && (e.KeyChar < 48 || e.KeyChar > 50)))
                e.Handled = true;
            else if (e.KeyChar != 8 && (e.KeyChar < 48 || e.KeyChar > 57))
                e.Handled = true;
        }

        private void genreYearTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (genreYearTextBox.Text.Length == 4 && e.KeyChar != 8) e.Handled = true;
            else if (genreYearTextBox.Text.Length == 0 && (e.KeyChar != 8 && (e.KeyChar < 48 || e.KeyChar > 50)))
                e.Handled = true;
            else if (e.KeyChar != 8 && (e.KeyChar < 48 || e.KeyChar > 57))
                e.Handled = true;
        }
#endregion

        private void quickAddArtistButton_Click(object sender, EventArgs e)
        {
            if (bandNameTextBox.Text == "" || bandYearTextBox.Text == "" || bandGenreComboBox.Text == "")
            {
                MessageBox.Show("Для создания группы необходимо задать\n\"Имя\", \"Год создания\" и \"Жанр\"!",
                    "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            string name = bandNameTextBox.Text,
                genre = bandGenreComboBox.Text;
            int id,
                gid = (int)this.genreTableTableAdapter.GetIdByName(bandGenreComboBox.Text),
                year = int.Parse(bandYearTextBox.Text);
            
            if (this.bandTableBindingSource.Find("BandName", name) != -1 &&
                this.bandTableBindingSource.Find("BandYear", year) != -1 &&
                this.bandTableBindingSource.Find("BandGenre", genre) != -1)
            {
                MessageBox.Show("Такой исполнитель уже есть в базе!", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }


            if (this.bandTableBindingSource.Count == 0)
            {
                id = 1;
            }
            else
            {
                id = (int)this.bandTableTableAdapter.GetLastID() + 1;
            }

            this.bandTableTableAdapter.Insert(id, gid, name, year, genre, 0,"","","");
            this.bandTableTableAdapter.Update(this.mLADataBaseDataSet.BandTable);

            this.bandTableTableAdapter.Fill(this.mLADataBaseDataSet.BandTable);

            bandNameTextBox.Text = "";
            bandYearTextBox.Text = "";
            albumAddNewButton.Enabled = true;
        }

        private void quickAddNewAlbumButton_Click(object sender, EventArgs e)
        {
            if (albumNameTextBox.Text == "" || albumYearTextBox.Text == "" || albumArtistComboBox.Text == "" ||
                albumGenreComboBox.Text == "")
            {
                MessageBox.Show("Для создания альбома необходимо задать\n\"Имя\", \"Год\", \"Жанр\" и исполнителя!",
                    "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (bandTableBindingSource.Find("BandName", albumArtistComboBox.Text) == -1)
            {
                MessageBox.Show("Исполнитель не найден в базе! Пожалуйста, проверьте\nправильность написания или создайте нового",
                    "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            string name = albumNameTextBox.Text,
                artist = albumArtistComboBox.Text,
                genre = albumGenreComboBox.Text;
            int id,
                bid = bandTableBindingSource.Find("BandName", albumArtistComboBox.Text) + 1,
                gid = genreTableBindingSource.Find("GenreName", albumGenreComboBox.Text) + 1,
                year = int.Parse(albumYearTextBox.Text);

            if (this.albumTableBindingSource.Find("AlbumName", name) != -1 &&
                this.albumTableBindingSource.Find("AlbumArtist", artist) != -1 &&
                this.albumTableBindingSource.Find("AlbumYear", year) != -1)
            {
                MessageBox.Show("Данный альбом уже есть в базе!", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (this.albumTableBindingSource.Count == 0)
            {
                id = 1;
            }
            else
            {
                id = (int)this.albumTableTableAdapter.GetLastID() + 1;
            }

            this.albumTableTableAdapter.Insert(id, bid, gid, name, year, genre, "", artist, "","");
            this.albumTableTableAdapter.Update(this.mLADataBaseDataSet.AlbumTable);
            this.albumTableTableAdapter.Fill(this.mLADataBaseDataSet.AlbumTable);

            albumNameTextBox.Text = "";
            albumYearTextBox.Text = "";

            string bandName = bandsOnMainFormDGV.Rows[bid - 1].Cells[2].Value.ToString(),
                bandGenre = bandsOnMainFormDGV.Rows[bid - 1].Cells[4].Value.ToString();
            int bandId = int.Parse(bandsOnMainFormDGV.Rows[bid - 1].Cells[0].Value.ToString()),
                bandGid = int.Parse(bandsOnMainFormDGV.Rows[bid - 1].Cells[1].Value.ToString()),
                bandYear = int.Parse(bandsOnMainFormDGV.Rows[bid - 1].Cells[3].Value.ToString()),
                bandAlbumsInDB = int.Parse(bandsOnMainFormDGV.Rows[bid - 1].Cells[5].Value.ToString());
            bandAlbumsInDB++;

            this.bandTableTableAdapter.UpdateAlbumsCount(bandAlbumsInDB, bandId);
            this.bandTableTableAdapter.Fill(this.mLADataBaseDataSet.BandTable);
        }

        private void addNewGenreButton_Click(object sender, EventArgs e)
        {
            if (bandNewGenreTextBox.Text == "")
            {
                MessageBox.Show("Введите название жанра!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            string name = bandNewGenreTextBox.Text;
            int id;
            id = this.genreTableBindingSource.Count + 1;
            this.genreTableTableAdapter.Insert(id, name, 0);
            this.genreTableTableAdapter.Update(this.mLADataBaseDataSet.GenreTable);
            this.genreTableTableAdapter.Fill(this.mLADataBaseDataSet.GenreTable);

            bandNewGenreTextBox.Text = "";
        }

        private void albumAddNewGenreButton_Click(object sender, EventArgs e)
        {
            if (albumAddNewGenreTextBox.Text == "")
            {
                MessageBox.Show("Введите название жанра!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            string name = albumAddNewGenreTextBox.Text;
            int id;
            id = this.mLADataBaseDataSet.GenreTable.Rows.Count + 1;
            this.genreTableTableAdapter.Insert(id, name, 0);
            this.genreTableTableAdapter.Update(this.mLADataBaseDataSet.GenreTable);
            this.genreTableTableAdapter.Fill(this.mLADataBaseDataSet.GenreTable);

            albumAddNewGenreTextBox.Text = "";
        }

        private void saveBandSearchResults_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Текстовый файл|*.txt";
            if (sfd.ShowDialog() == DialogResult.Cancel) return;
            FileStream fs = new FileStream(sfd.FileName, FileMode.Create);
            StreamWriter sw = new StreamWriter(fs);
            sw.WriteLine("|ID |       Исполнитель       |Год |      Жанр      |Альбомов|");
            for (int i = 0; i < bandsOnMainFormDGV.RowCount; i++)
            {
                sw.WriteLine("|{0,3}|{1,-25}|{2,-4}|{3,-16}|{4,-8}|",
                    bandsOnMainFormDGV.Rows[i].Cells[0].Value.ToString(),
                    bandsOnMainFormDGV.Rows[i].Cells[2].Value.ToString(),
                    bandsOnMainFormDGV.Rows[i].Cells[3].Value.ToString(),
                    bandsOnMainFormDGV.Rows[i].Cells[4].Value.ToString(),
                    bandsOnMainFormDGV.Rows[i].Cells[5].Value.ToString());
            }
            sw.Close();
            fs.Close();
            MessageBox.Show("Информация успешно записана!", "Успех!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void showArtistInfoButton_Click(object sender, EventArgs e)
        {
            string bandName;
            int id;

            try
            {
                bandName = bandsOnMainFormDGV.CurrentRow.Cells[2].Value.ToString();
            }
            catch
            {
                MessageBox.Show("Сначала выберете группу, у которой хотите просмотреть/изменить информацию!",
                    "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            id = int.Parse(bandsOnMainFormDGV.CurrentRow.Cells[0].Value.ToString());

            BandInfo form = new BandInfo(id, bandName);
            form.AlbumsInDBTotal = this.albumTableBindingSource.Count;

            switch (form.ShowDialog())
            {
                case DialogResult.Abort:
                    {
                        deleteArtistButton_Click(int.Parse(form.bandIDlabel.Text));
                        return;
                    }
                case DialogResult.Cancel:
                    {
                        return;
                    }
                case DialogResult.OK:
                    {
                        FillAllTables();
                        break;
                    }
            } 
        }

        private void showAlbumInfoButton_Click(object sender, EventArgs e)
        {
            string albumName;
            int aID;

            try
            {
                albumName = albumsOnMainFormDGV.CurrentRow.Cells[3].Value.ToString();
            }
            catch
            {
                MessageBox.Show("Сначала выберете альбом, у которого хотите просмотреть/изменить информацию!",
                    "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            aID = int.Parse(albumsOnMainFormDGV.CurrentRow.Cells[0].Value.ToString());

            AlbumInfo form = new AlbumInfo(aID, albumName);

            switch (form.ShowDialog())
            {
                case DialogResult.Abort:
                    {
                        deleteAlbumButton_Click(int.Parse(form.albumIDlabel.Text));
                        return;
                    }
                case DialogResult.Cancel:
                    {
                        return;
                    }
                case DialogResult.OK:
                    {
                        FillAllTables();
                        break;
                    }
            } 
        }

        private void bandsOnMainFormDGV_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (bandsOnMainFormDGV.CurrentCell.ColumnIndex == 5)
            {
                if (bandsOnMainFormDGV.CurrentCell.Value.ToString() == "0") return;
                int BID = int.Parse(bandsOnMainFormDGV.CurrentRow.Cells[0].Value.ToString());
                this.albumTableTableAdapter.FillBy(mLADataBaseDataSet.AlbumTable, BID);

                tabControl1.SelectedIndex = 1;
                albumClearSearchResultsButton.Enabled = true;
                albumAddNewButton.Enabled = false;
            }

        }

        private void albumClearSearchResults_Click(object sender, EventArgs e)
        {
            this.albumTableTableAdapter.Fill(this.mLADataBaseDataSet.AlbumTable);
            albumClearSearchResultsButton.Enabled = false;
            albumSaveSearchResultButton.Enabled = false;
            albumAddNewButton.Enabled = true;
        }

        private void bandsOnMainFormDGV_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            bandsOnMainFormDGV.Rows[bandsOnMainFormDGV.CurrentCell.RowIndex].Selected = true;
        }

        private void deleteArtistButton_Click(object sender, EventArgs e)
        {
            if (this.bandTableBindingSource.Count == 0)
            {
                MessageBox.Show("База пуста!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                albumAddNewButton.Enabled = false;
                return;
            }

            List<int> aids = new List<int>();
            int id = int.Parse(bandsOnMainFormDGV.CurrentRow.Cells[0].Value.ToString());

            for (int i = 0; i < albumsOnMainFormDGV.RowCount; i++)
            {
                if (int.Parse(albumsOnMainFormDGV.Rows[i].Cells[1].Value.ToString()) == id)
                    aids.Add(int.Parse(albumsOnMainFormDGV.Rows[i].Cells[0].Value.ToString()));
            }

            for (int i = 0; i < aids.Count; i++)
            {
                this.albumTableTableAdapter.DeleteSongsById(aids[i]);
            }

            this.bandTableTableAdapter.DeleteByID(id);
            this.bandTableTableAdapter.Update(mLADataBaseDataSet.BandTable);
            this.bandTableTableAdapter.Fill(this.mLADataBaseDataSet.BandTable);
            this.albumTableTableAdapter.Fill(this.mLADataBaseDataSet.AlbumTable);
        }

        private void deleteArtistButton_Click(int id)
        {
            if (this.bandTableBindingSource.Count == 0)
            {
                MessageBox.Show("База пуста!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                albumAddNewButton.Enabled = false;
                return;
            }

            List<int> aids = new List<int>();

            for (int i = 0; i < albumsOnMainFormDGV.RowCount; i++)
            {
                if (int.Parse(albumsOnMainFormDGV.Rows[i].Cells[1].Value.ToString()) == id)
                    aids.Add(int.Parse(albumsOnMainFormDGV.Rows[i].Cells[0].Value.ToString()));
            }

            for (int i = 0; i < aids.Count; i++)
            {
                this.albumTableTableAdapter.DeleteSongsById(aids[i]);
            }
 
            this.bandTableTableAdapter.DeleteByID(id);
            this.bandTableTableAdapter.Update(mLADataBaseDataSet.BandTable);
            this.bandTableTableAdapter.Fill(this.mLADataBaseDataSet.BandTable);
            this.albumTableTableAdapter.Fill(this.mLADataBaseDataSet.AlbumTable);
        }

        private void deleteAlbumButton_Click(object sender, EventArgs e)
        {
            if (this.albumTableBindingSource.Count == 0)
            {
                MessageBox.Show("База альбомов пуста!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                albumAddNewButton.Enabled = false;
                return;
            }

            int id = int.Parse(albumsOnMainFormDGV.CurrentRow.Cells[0].Value.ToString()),
                albumsCount = (int)

            this.bandTableTableAdapter.UpdateAlbumsCount((int)this.bandTableTableAdapter.GetAlbumsCountByID(int.Parse(albumsOnMainFormDGV.CurrentRow.Cells[1].Value.ToString()))-1, int.Parse(albumsOnMainFormDGV.CurrentRow.Cells[1].Value.ToString()));
            this.bandTableTableAdapter.Update(this.mLADataBaseDataSet.BandTable);
            this.albumTableTableAdapter.DeleteSongsById(id);
            this.albumTableTableAdapter.DeleteByID(id);
            this.albumTableTableAdapter.Update(mLADataBaseDataSet.AlbumTable);

            this.albumTableTableAdapter.Fill(this.mLADataBaseDataSet.AlbumTable);
            this.bandTableTableAdapter.Fill(this.mLADataBaseDataSet.BandTable);
        }

        private void deleteAlbumButton_Click(int id)
        {
            if (this.albumTableBindingSource.Count == 0)
            {
                MessageBox.Show("База альбомов пуста!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                albumAddNewButton.Enabled = false;
                return;
            }

            this.albumTableTableAdapter.DeleteSongsById(id);
            this.albumTableTableAdapter.DeleteByID(id);
            this.albumTableTableAdapter.Update(mLADataBaseDataSet.AlbumTable);
            this.albumTableTableAdapter.Fill(this.mLADataBaseDataSet.AlbumTable);
        }

        private void genresOnMainFormDGV_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            genreChangeDataButton.Enabled = true;
            int id = int.Parse(genresOnMainFormDGV.CurrentRow.Cells[0].Value.ToString());
            genreNameTextBox.Text = this.genreTableTableAdapter.GetNameByID(id);
            genreYearTextBox.Text = this.genreTableTableAdapter.GetYearByID(id).ToString();
        }

        private void genreAddNewButton_Click(object sender, EventArgs e)
        {
            if (genreNameTextBox.Text == "")
            {
                MessageBox.Show("Для добавления жанра необходимо задать имя!",
                    "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            string name = genreNameTextBox.Text;
            int year, id;

            year = genreYearTextBox.Text != "" ? int.Parse(genreYearTextBox.Text) : 0;

            if (this.genreTableBindingSource.Count == 0)
            {
                id = 1;
            }
            else
            {
                id = (int)this.genreTableTableAdapter.GetLastID() + 1;
            }

            this.genreTableTableAdapter.Insert(id, name, year);
            this.genreTableTableAdapter.Update(this.mLADataBaseDataSet.GenreTable);

            this.genreTableTableAdapter.Fill(this.mLADataBaseDataSet.GenreTable);

            genreNameTextBox.Text = "";
            genreYearTextBox.Text = "";
            genreDeleteDataButton.Enabled = true;
            genreChangeDataButton.Enabled = true;
        }

        private void genreChangeDataButton_Click(object sender, EventArgs e)
        {
            if (genreNameTextBox.Text == "" || genreYearTextBox.Text == "")
            {
                MessageBox.Show("Для добавления жанра необходимо задать\n\"Имя\" и \"Год создания\"!",
                    "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            string name = genreNameTextBox.Text;
            int year = int.Parse(genreYearTextBox.Text),
                id = int.Parse(genresOnMainFormDGV.CurrentRow.Cells[0].Value.ToString());

            this.genreTableTableAdapter.UpdateByID(name, year, id);
            this.genreTableTableAdapter.Update(this.mLADataBaseDataSet.GenreTable);

            this.genreTableTableAdapter.Fill(this.mLADataBaseDataSet.GenreTable);

            genreNameTextBox.Text = "";
            genreYearTextBox.Text = "";
        }

        private void genreDeleteDataButton_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Все группы и альбомы, а так же привязанные к ним песни, имеющий этот жанр, "+
                "будут тоже удалены. Вы уверены, что хотите продолжить?", 
                "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;
            else
            {
                int id = int.Parse(genresOnMainFormDGV.CurrentRow.Cells[0].Value.ToString());
                
                this.genreTableTableAdapter.DeleteByID(id);
                this.genreTableTableAdapter.Update(this.mLADataBaseDataSet.GenreTable);
                this.genreTableTableAdapter.Fill(this.mLADataBaseDataSet.GenreTable);
            }
        }

        private void searchBandsButton_Click(object sender, EventArgs e)
        {
            int firstYear = 0, lastYear = 0;
            if (bandSearchNameCheckBox.Checked == true && bandSearchNameTextBox.Text == "") bandSearchNameCheckBox.Checked = false;
            if (bandSearchYearCheckBox.Checked == true && bandFirstYearTextBox.Text == "" && bandLastYearTextBox.Text == "") bandSearchYearCheckBox.Checked = false;
            else
            {
                if (bandFirstYearTextBox.Text == "") firstYear = 0;
                else firstYear = int.Parse(bandFirstYearTextBox.Text);
                if (bandLastYearTextBox.Text == "") lastYear = 9999;
                else lastYear = int.Parse(bandLastYearTextBox.Text);
            }
            if (bandSearchGenreCheckBox.Checked == true && bandSearchGenreComboBox.Text == "") bandSearchGenreCheckBox.Checked = false;

            if (bandSearchNameCheckBox.Checked == true) 
            {
                        this.bandTableTableAdapter.FillByName(this.mLADataBaseDataSet.BandTable,bandSearchNameTextBox.Text);
                        bandSearchClearButton.Enabled = true;
                saveBandSearchResults.Enabled = true;
                
            }
            else if (bandSearchGenreCheckBox.Checked == true)
            {
                // Жанр с годом
                if (bandSearchYearCheckBox.Checked == true)
                {
                    this.bandTableTableAdapter.FillByYearAndGenre(this.mLADataBaseDataSet.BandTable,
                        firstYear, lastYear, bandSearchGenreComboBox.Text);
                    bandSearchClearButton.Enabled = true;
                    saveBandSearchResults.Enabled = true;
                } 
                else
                {   // жанр без года
                    this.bandTableTableAdapter.FillByGenre(this.mLADataBaseDataSet.BandTable,
                        bandSearchGenreComboBox.Text);
                    bandSearchClearButton.Enabled = true;
                    saveBandSearchResults.Enabled = true;
                }
            }
            else
            {   // только год
                this.bandTableTableAdapter.FillByYear(this.mLADataBaseDataSet.BandTable, firstYear, lastYear);
                bandSearchClearButton.Enabled = true;
                saveBandSearchResults.Enabled = true;
            }
        }

        private void albumSearchButton_Click(object sender, EventArgs e)
        {
            int firstYear = 0, lastYear = 0;
            if (albumSearchNameCheckBox.Checked == true && albumSearchNameTextBox.Text == "") albumSearchNameCheckBox.Checked = false;
            if (albumSearchYearCheckBox.Checked == true && albumFirstYearTextBox.Text == "" && albumLastYearTextBox.Text == "") albumSearchYearCheckBox.Checked = false;
            else
            {
                if (albumFirstYearTextBox.Text == "") firstYear = 0;
                else firstYear = int.Parse(albumFirstYearTextBox.Text);
                if (albumLastYearTextBox.Text == "") lastYear = 9999;
                else lastYear = int.Parse(albumLastYearTextBox.Text);
            }
            if (albumSearchGenreCheckBox.Checked == true && albumSearchGenreComboBox.Text == "") albumSearchGenreCheckBox.Checked = false;
            if (albumSearchArtistCheckBox.Checked == true && albumSearchBandComboBox.Text == "") albumSearchArtistCheckBox.Checked = false;

            if (albumSearchNameCheckBox.Checked == true)
            {
                this.albumTableTableAdapter.FillByAlbumName(this.mLADataBaseDataSet.AlbumTable, albumSearchNameTextBox.Text);
                albumSearchButton.Enabled = true;
                albumSaveSearchResultButton.Enabled = true;

            }
            else if (albumSearchGenreCheckBox.Checked == true)
            {
                // Жанр с годом
                if (albumSearchYearCheckBox.Checked == true)
                {
                    // С исполнителем
                    if (albumSearchArtistCheckBox.Checked == true)
                    {
                        this.albumTableTableAdapter.FillByYearArtistGenre(this.mLADataBaseDataSet.AlbumTable,
                            firstYear, lastYear, albumSearchBandComboBox.Text, albumGenreComboBox.Text);
                        albumSearchButton.Enabled = true;
                        albumSaveSearchResultButton.Enabled = true;
                    }
                    // Без исполнителя
                    else
                    {
                        this.albumTableTableAdapter.FillByGenreAndYear(this.mLADataBaseDataSet.AlbumTable,
                            firstYear, lastYear, albumGenreComboBox.Text);
                        albumSearchButton.Enabled = true;
                        albumSaveSearchResultButton.Enabled = true;
                    }
                }
                // Жанр без года
                else
                {   // С исполнителем
                    if (albumSearchArtistCheckBox.Checked == true)
                    {
                        this.albumTableTableAdapter.FillByArtistAndGenre(this.mLADataBaseDataSet.AlbumTable,
                            albumGenreComboBox.Text, albumSearchBandComboBox.Text);
                        albumSearchButton.Enabled = true;
                        albumSaveSearchResultButton.Enabled = true;
                    }
                    // Без исполнителя
                    else
                    {
                        this.albumTableTableAdapter.FillByGenre(this.mLADataBaseDataSet.AlbumTable,
                            albumArtistComboBox.Text);
                        albumSearchButton.Enabled = true;
                        albumSaveSearchResultButton.Enabled = true;
                    }
                }
            }
            //Без жанра
            else
            {   // С годом
                if (albumSearchYearCheckBox.Checked == true)
                {
                    // С исполнителем
                    if (albumSearchArtistCheckBox.Checked == true)
                    {
                        this.albumTableTableAdapter.FillByYearAndArtist(this.mLADataBaseDataSet.AlbumTable,
                            firstYear, lastYear, albumSearchBandComboBox.Text);
                        albumSearchButton.Enabled = true;
                        albumSaveSearchResultButton.Enabled = true;
                    }
                    // Без Исполнителя
                    else
                    {
                        this.albumTableTableAdapter.FillByYear(this.mLADataBaseDataSet.AlbumTable,
                            firstYear, lastYear);
                        albumSearchButton.Enabled = true;
                        albumSaveSearchResultButton.Enabled = true;
                    }
                }
                // Без года
                else
                {
                    this.albumTableTableAdapter.FillByArtist(this.mLADataBaseDataSet.AlbumTable,
                        albumSearchBandComboBox.Text);
                    albumSearchButton.Enabled = true;
                    albumSaveSearchResultButton.Enabled = true;
                }
            }
            albumClearSearchResultsButton.Enabled = true;
        }

        private void albumSaveSearchResultButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Текстовый файл|*.txt";
            if (sfd.ShowDialog() == DialogResult.Cancel) return;
            FileStream fs = new FileStream(sfd.FileName, FileMode.Create);
            StreamWriter sw = new StreamWriter(fs);
            sw.WriteLine("|ID |       Исполнитель       |          Альбом          |      Жанр      |Год |      Лейбл     |Длинна|");
            for (int i = 0; i < albumsOnMainFormDGV.RowCount; i++)
            {
                sw.WriteLine("|{0,3}|{1,-25}|{2,-26}|{3,-16}|{4,-4}|{5,-16}|{6,-6}|",
                    albumsOnMainFormDGV.Rows[i].Cells[0].Value.ToString(),
                    albumsOnMainFormDGV.Rows[i].Cells[4].Value.ToString(),
                    albumsOnMainFormDGV.Rows[i].Cells[3].Value.ToString(),
                    albumsOnMainFormDGV.Rows[i].Cells[6].Value.ToString(),
                    albumsOnMainFormDGV.Rows[i].Cells[5].Value.ToString(),
                    albumsOnMainFormDGV.Rows[i].Cells[7].Value.ToString(),
                    albumsOnMainFormDGV.Rows[i].Cells[8].Value.ToString());
            }
            sw.Close();
            fs.Close();
            MessageBox.Show("Информация успешно записана!", "Успех!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void addArtistToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BandInfo form = new BandInfo();
            form.AlbumsInDBTotal = this.albumTableBindingSource.Count;

            switch (form.ShowDialog())
            {
                case DialogResult.Abort:
                    {
                        deleteArtistButton_Click(int.Parse(form.bandIDlabel.Text));
                        return;
                    }
                case DialogResult.Cancel:
                    {
                        return;
                    }
                case DialogResult.OK:
                    {
                        FillAllTables();
                        break;
                    }
            } 
        }

        private void addGenreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 2;
        }

        private void addAlbumToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AlbumInfo form = new AlbumInfo();

            switch (form.ShowDialog())
            {
                case DialogResult.Abort:
                    {
                        deleteAlbumButton_Click(int.Parse(form.albumIDlabel.Text));
                        return;
                    }
                case DialogResult.Cancel:
                    {
                        return;
                    }
                case DialogResult.OK:
                    {
                        FillAllTables();
                        break;
                    }
            } 
        }

        private void updateDBToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.bandTableBindingSource.EndEdit();
            this.albumTableBindingSource.EndEdit();
            this.genreTableBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.mLADataBaseDataSet);

            FillAllTables();
        }

        private void searchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SearchForm form = new SearchForm();
            form.ShowDialog();
        }

        private void clearDBToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.genreTableBindingSource.Count == 0)
            {
                MessageBox.Show("База пуста!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (MessageBox.Show("Вы уверены, что хотите очистить всю базу?\nВнимание, восстановить базу будет нельзя!",
                "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;
            else
            {
                this.songTableTableAdapter.DeleteAll();
                this.songTableTableAdapter.Update(this.mLADataBaseDataSet.SongTable);
                this.albumTableTableAdapter.DeleteAll();
                this.albumTableTableAdapter.Update(this.mLADataBaseDataSet.AlbumTable);
                this.bandTableTableAdapter.DeleteAll();
                this.bandTableTableAdapter.Update(this.mLADataBaseDataSet.BandTable);
                this.genreTableTableAdapter.DeleteAll();
                this.genreTableTableAdapter.Update(this.mLADataBaseDataSet.GenreTable);
                FillAllTables();
                genreNameTextBox.Text = "";
                genreYearTextBox.Text = "";
            }
        }

    }
}
