﻿using System;
using System.Windows.Forms;
using System.Text;
using System.Net;

namespace MusicLibraryApplication
{
    class Music
    {
        public static string DownloadInfo(string name, out string linkToAvatar)
        {
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            string content, image, path = name;
            int startIndex, endIndex;

            path = path.Replace(' ', '+');
            // Путь к странице групы на Lastfm
            path = @"http://www.lastfm.ru/music/" + path;

            try
            {   
                // Скачиваем всю страницу
                content = client.DownloadString(path);
            }
            catch
            {
                MessageBox.Show("К сожалению, произошла ошибка. Возможно, у вас отсутсвует подключение к интернету или сайт LastFM временно неработает. Дополнительная информация о группе пока недоступна.",
                    "Произошла ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                linkToAvatar = "";
                return "error";
            }

            // Выцепляем картинку
            image = content.Remove(0, content.IndexOf(@"http://userserve-ak.last.fm")+50);
            image = image.Remove(0, image.IndexOf(@"http://userserve-ak.last.fm"));
            image = image.Replace(".png", ".jpg");
            image = image.Substring(0, image.IndexOf(".jpg") + 4);

            startIndex = content.IndexOf("<div class=\"wiki-text\">") + 27;
            endIndex = content.IndexOf("<div class=\"wiki-options\">") - 18;
            content = content.Substring(startIndex, endIndex - startIndex);

            content = content.Remove(content.IndexOf("<span class=\"hide\">"), 19);
            content = HTMLParser.Parse(content);

            linkToAvatar = image;
            return content;
        }

        public static int TimeParse(string length)
        {
            int result = 0;
            if (length == "  :" || length == "") return 0;
            result += int.Parse(length.Substring(3, 2));
            result += 60 * int.Parse(length.Substring(0, 2));
            return result;
        }

        public static string TimeParse(int lengthInSeconds)
        {
            if (lengthInSeconds / 60 < 10 && lengthInSeconds % 60 < 10) return string.Format("0" + (lengthInSeconds / 60).ToString() + ":0" + (lengthInSeconds % 60).ToString());
            else if (lengthInSeconds / 60 < 10) return string.Format("0" + (lengthInSeconds / 60).ToString() + ":" + (lengthInSeconds % 60).ToString());
            else if (lengthInSeconds % 60 < 10) return string.Format((lengthInSeconds / 60).ToString() + ":0" + (lengthInSeconds % 60).ToString());
            else return string.Format((lengthInSeconds / 60).ToString() + ":" + (lengthInSeconds % 60).ToString());
        }
    }
}
