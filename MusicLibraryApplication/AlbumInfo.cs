﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using TagLib;

namespace MusicLibraryApplication
{
    public partial class AlbumInfo : Form
    {
        public AlbumInfo()
        {
            InitializeComponent();
            this.albumTableTableAdapter.Fill(this.mLADataBaseDataSet.AlbumTable);
            this.bandTableTableAdapter.Fill(this.mLADataBaseDataSet.BandTable);
            this.genreTableTableAdapter.Fill(this.mLADataBaseDataSet.GenreTable);
            this.genreTableBindingSource.Sort = "GenreName ASC";

            albumIDlabel.Text = (this.albumTableBindingSource.Count + 1).ToString();
            deleteCoverButton.Enabled = false;
            if (songsDGV.RowCount != 0) deleteSongButton.Enabled = true;
            else deleteSongButton.Enabled = false;

            NewAlbum = true;
        }

        public AlbumInfo(int id, string albumName)
        {
            InitializeComponent();

            this.genreTableTableAdapter.Fill(this.mLADataBaseDataSet.GenreTable);
            this.bandTableTableAdapter.Fill(this.mLADataBaseDataSet.BandTable);
            this.genreTableBindingSource.Sort = "GenreName ASC";

            

            albumIDlabel.Text = id.ToString();
            albumNameTextBox.Text = albumName;
            albumBandComboBox.Text = (string)this.albumTableTableAdapter.GetAuthorByID(id);
            albumYearTextBox.Text = this.albumTableTableAdapter.GetYearByID(id).ToString();
            albumGenreComboBox.Text = (string)this.albumTableTableAdapter.GetGenreByID(id);
            albumLabelTextBox.Text = this.albumTableTableAdapter.GetLabelByID(id);
            albumLengthMaskedTextBox.Text = this.albumTableTableAdapter.GetLengthByID(id);
            coverPictureBox.ImageLocation = this.albumTableTableAdapter.GetCoverPathByID(id);
            if (coverPictureBox.ImageLocation == null || coverPictureBox.ImageLocation == "") deleteCoverButton.Enabled = false;
            else deleteCoverButton.Enabled = true;
            
            this.songTableTableAdapter.FillBy(this.mLADataBaseDataSet.SongTable, id);
            if (songsDGV.RowCount != 0) deleteSongButton.Enabled = true;
            else deleteSongButton.Enabled = false;

            if (!Options.ifAdmin)
            {
                saveAlbumButton.Enabled = false;
                chooseCoverButton.Enabled = false;
                deleteAlbumButton.Enabled = false;
                deleteCoverButton.Enabled = false;
                addSongButton.Enabled = false;
                deleteSongButton.Enabled = false;
                scanFolderButton.Enabled = false;
            }

            NewAlbum = false;
        }

        public bool NewAlbum;

        private void scanFolderButton_Click(object sender, EventArgs e)
        {
            if (NewAlbum)
            {
                MessageBox.Show("Сначала сохраните данные об альбоме в базу",
                    "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            MessageBox.Show("Пожалуйста учтите, что сканируемые файлы должны быть в формате MP3!", "Предупреждение",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
            int lengthInSeconds = 0;
            string fullPath; // Путь к папке
            FolderBrowserDialog fbd = new FolderBrowserDialog();

            // Выбираем директорию
            if (fbd.ShowDialog() == DialogResult.Cancel) return;
            string length;
            fullPath = fbd.SelectedPath;

            try
            {
                TagLib.File mp3File;

                string[] fullfilesPath = Directory.GetFiles(fullPath, "*.mp3*", SearchOption.AllDirectories);
                for (int i = 0; i < fullfilesPath.Length; i++)
                {
                    mp3File = TagLib.File.Create(fullfilesPath[i]);
                    length = mp3File.Properties.Duration.ToString().Substring(3, 5);
                    lengthInSeconds += Music.TimeParse(length);
                    this.songTableTableAdapter.Insert((int)mp3File.Tag.Track,
                        mp3File.Tag.Title,
                        length,
                        int.Parse(albumIDlabel.Text));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                MessageBox.Show("Произошла ошибка при сканировании папки.\nПожалуйста, убедитесь что все файлы в папке имеют формат MP3",
                    "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            albumLengthMaskedTextBox.Text = Music.TimeParse(lengthInSeconds);
            deleteSongButton.Enabled = true;

            this.songTableTableAdapter.Update(this.mLADataBaseDataSet.SongTable);
            this.songTableTableAdapter.FillBy(this.mLADataBaseDataSet.SongTable, int.Parse(albumIDlabel.Text));
        }

        private void saveAlbumButton_Click(object sender, EventArgs e)
        {
            if (albumNameTextBox.Text == "" || albumYearTextBox.Text == "" || albumGenreComboBox.Text == "")
            {
                MessageBox.Show("Для создания альбома необходимо задать\n\"Имя\", \"Год создания\" и \"Жанр\"!",
                    "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            string name = albumNameTextBox.Text,
                genre = albumGenreComboBox.Text,
                band = albumBandComboBox.Text,
                label = albumLabelTextBox.Text,
                length = albumLengthMaskedTextBox.Text,
                pathToCover = coverPictureBox.ImageLocation;
            int ID = int.Parse(albumIDlabel.Text),
                bID = (int)this.bandTableTableAdapter.GetIDByBandName(albumBandComboBox.Text),
                gID = this.genreTableBindingSource.Find("GenreName", genre) + 1,
                year = int.Parse(albumYearTextBox.Text);

            if (NewAlbum)
            {
                this.albumTableTableAdapter.Insert(ID, bID, gID, name, year, genre, label, band, length, pathToCover);
                this.albumTableTableAdapter.Update(this.mLADataBaseDataSet.AlbumTable);
            }
            else
            {
                this.albumTableTableAdapter.UpdateAllByID(bID, gID, name, year, genre, label, band, length,pathToCover, ID);
                this.albumTableTableAdapter.Update(this.mLADataBaseDataSet.AlbumTable);
            }
        }

        private void albumYearTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (albumYearTextBox.Text.Length == 4 && e.KeyChar != 8) e.Handled = true;
            else if (albumYearTextBox.Text.Length == 0 && (e.KeyChar != 8 && (e.KeyChar < 48 || e.KeyChar > 50)))
                e.Handled = true;
            else if (e.KeyChar != 8 && (e.KeyChar < 48 || e.KeyChar > 57))
                e.Handled = true;
        }

        private void addSongButton_Click(object sender, EventArgs e)
        {
            
            if ((songNameTextBox.Text == "" || songNameTextBox.Text == "Введите название песни") && songLengthTextBox.Text == "  :")
            {
                MessageBox.Show("Для добавления песни необходимо задать\nназвание и длительность!",
                    "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            string name, length;
            int number, aid;

            name = songNameTextBox.Text;
            length = songLengthTextBox.Text;
            number = songsDGV.RowCount + 1;
            aid = int.Parse(albumIDlabel.Text);

            this.songTableTableAdapter.Insert(number, name, length, aid);
            this.songTableTableAdapter.Update(this.mLADataBaseDataSet.SongTable);
            this.songTableTableAdapter.FillBy(this.mLADataBaseDataSet.SongTable, int.Parse(albumIDlabel.Text));

            int totalLength = Music.TimeParse(length) + Music.TimeParse(albumLengthMaskedTextBox.Text);
            albumLengthMaskedTextBox.Text = Music.TimeParse(totalLength);

            songNameTextBox.Text = "Введите название песни";
            songLengthTextBox.Text = "  :";
            deleteSongButton.Enabled = true;
        }

        private void songNameTextBox_TextChanged(object sender, EventArgs e)
        {
            if (songNameTextBox.Text == "Введите название песни") songNameTextBox.Text = "";
        }

        private void chooseCoverTextBox_Click(object sender, EventArgs e)
        {
            OpenFileDialog opf = new OpenFileDialog();
            string path, newFileName;

            opf.Filter = "Изображения (*.jpg, *.png, *.bmp)|*.jpg;*.png;*.bmp";
            if (opf.ShowDialog() == DialogResult.Cancel) return;
            path = opf.FileName;
            newFileName = albumIDlabel.Text + "_" + albumNameTextBox.Text + path.Substring(path.Length - 4);

            FileStream inputFile;
            FileStream outputFile;

            inputFile = new FileStream(path, FileMode.Open);

            //Теперь надо создать копию
            outputFile = new FileStream(newFileName, FileMode.Create);

            //Теперь копирование
            int i;
            do
            {
                i = inputFile.ReadByte();
                if (i != -1)
                {
                    outputFile.WriteByte((byte)i);
                }
            }
            while (i != -1);

            inputFile.Close();
            outputFile.Close();

            coverPictureBox.ImageLocation = newFileName;
            deleteCoverButton.Enabled = true;
        }

        private void albumNameTextBox_TextChanged(object sender, EventArgs e)
        {
            chooseCoverButton.Enabled = albumNameTextBox.Text != "" ? true : false;
        }

        private void deleteCoverButton_Click(object sender, EventArgs e)
        {
            System.IO.File.Delete(coverPictureBox.ImageLocation);
            coverPictureBox.ImageLocation = null;
        }

        private void deleteAlbumButton_Click(object sender, EventArgs e)
        {
            if (NewAlbum) deleteAlbumButton.DialogResult = DialogResult.Cancel;
        }

        private void deleteSongButton_Click(object sender, EventArgs e)
        {
            int id = int.Parse(songsDGV.CurrentRow.Cells[0].Value.ToString());
            albumLengthMaskedTextBox.Text = Music.TimeParse(Music.TimeParse(albumLengthMaskedTextBox.Text) - 
                Music.TimeParse(songsDGV.CurrentRow.Cells[2].Value.ToString()));
            this.songTableTableAdapter.DeleteBySongNumber(id, songsDGV.CurrentRow.Cells[1].Value.ToString());
            this.songTableTableAdapter.Update(this.mLADataBaseDataSet.SongTable);
            this.songTableTableAdapter.FillBy(mLADataBaseDataSet.SongTable, int.Parse(albumIDlabel.Text));
            if (songsDGV.RowCount == 0) deleteSongButton.Enabled = false;

        }
    }
}
