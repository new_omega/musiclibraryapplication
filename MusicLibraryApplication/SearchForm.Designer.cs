﻿namespace MusicLibraryApplication
{
    partial class SearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SearchForm));
            this.genreRadioButton = new System.Windows.Forms.RadioButton();
            this.albumRadioButton = new System.Windows.Forms.RadioButton();
            this.bandRadioButton = new System.Windows.Forms.RadioButton();
            this.idTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.searchButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.linkTypeComboBox = new System.Windows.Forms.ComboBox();
            this.mLADataBaseDataSet = new MusicLibraryApplication.MLADataBaseDataSet();
            this.genreTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.genreTableTableAdapter = new MusicLibraryApplication.MLADataBaseDataSetTableAdapters.GenreTableTableAdapter();
            this.tableAdapterManager = new MusicLibraryApplication.MLADataBaseDataSetTableAdapters.TableAdapterManager();
            this.albumTableTableAdapter = new MusicLibraryApplication.MLADataBaseDataSetTableAdapters.AlbumTableTableAdapter();
            this.bandTableTableAdapter = new MusicLibraryApplication.MLADataBaseDataSetTableAdapters.BandTableTableAdapter();
            this.songTableTableAdapter = new MusicLibraryApplication.MLADataBaseDataSetTableAdapters.SongTableTableAdapter();
            this.genreTableDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bandTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bandTableDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.albumTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.albumTableDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.songTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.songTableDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mLADataBaseDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.genreTableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.genreTableDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandTableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandTableDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.albumTableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.albumTableDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.songTableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.songTableDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // genreRadioButton
            // 
            this.genreRadioButton.AutoSize = true;
            this.genreRadioButton.Location = new System.Drawing.Point(6, 19);
            this.genreRadioButton.Name = "genreRadioButton";
            this.genreRadioButton.Size = new System.Drawing.Size(54, 17);
            this.genreRadioButton.TabIndex = 5;
            this.genreRadioButton.TabStop = true;
            this.genreRadioButton.Text = "Жанр";
            this.genreRadioButton.UseVisualStyleBackColor = true;
            this.genreRadioButton.CheckedChanged += new System.EventHandler(this.genreRadioButton_CheckedChanged);
            // 
            // albumRadioButton
            // 
            this.albumRadioButton.AutoSize = true;
            this.albumRadioButton.Location = new System.Drawing.Point(132, 19);
            this.albumRadioButton.Name = "albumRadioButton";
            this.albumRadioButton.Size = new System.Drawing.Size(64, 17);
            this.albumRadioButton.TabIndex = 7;
            this.albumRadioButton.TabStop = true;
            this.albumRadioButton.Text = "Альбом";
            this.albumRadioButton.UseVisualStyleBackColor = true;
            this.albumRadioButton.CheckedChanged += new System.EventHandler(this.genreRadioButton_CheckedChanged);
            // 
            // bandRadioButton
            // 
            this.bandRadioButton.AutoSize = true;
            this.bandRadioButton.Location = new System.Drawing.Point(66, 19);
            this.bandRadioButton.Name = "bandRadioButton";
            this.bandRadioButton.Size = new System.Drawing.Size(60, 17);
            this.bandRadioButton.TabIndex = 8;
            this.bandRadioButton.TabStop = true;
            this.bandRadioButton.Text = "Группа";
            this.bandRadioButton.UseVisualStyleBackColor = true;
            this.bandRadioButton.CheckedChanged += new System.EventHandler(this.genreRadioButton_CheckedChanged);
            // 
            // idTextBox
            // 
            this.idTextBox.Location = new System.Drawing.Point(30, 42);
            this.idTextBox.Name = "idTextBox";
            this.idTextBox.Size = new System.Drawing.Size(166, 20);
            this.idTextBox.TabIndex = 9;
            this.idTextBox.TextChanged += new System.EventHandler(this.genreRadioButton_CheckedChanged);
            this.idTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.idTextBox_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "ID:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.genreRadioButton);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.albumRadioButton);
            this.groupBox1.Controls.Add(this.idTextBox);
            this.groupBox1.Controls.Add(this.bandRadioButton);
            this.groupBox1.Controls.Add(this.searchButton);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(280, 71);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Поиск по ID";
            // 
            // searchButton
            // 
            this.searchButton.Enabled = false;
            this.searchButton.Location = new System.Drawing.Point(202, 40);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(68, 23);
            this.searchButton.TabIndex = 12;
            this.searchButton.Text = "Искать!";
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(513, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Выберите тип связи:";
            // 
            // linkTypeComboBox
            // 
            this.linkTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.linkTypeComboBox.FormattingEnabled = true;
            this.linkTypeComboBox.Items.AddRange(new object[] {
            "Жанр -> Группы -> Альбомы",
            "Жанр -> Альбомы & Группы"});
            this.linkTypeComboBox.Location = new System.Drawing.Point(632, 12);
            this.linkTypeComboBox.Name = "linkTypeComboBox";
            this.linkTypeComboBox.Size = new System.Drawing.Size(199, 21);
            this.linkTypeComboBox.TabIndex = 14;
            this.linkTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.linkTypeComboBox_SelectedIndexChanged);
            // 
            // mLADataBaseDataSet
            // 
            this.mLADataBaseDataSet.DataSetName = "MLADataBaseDataSet";
            this.mLADataBaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // genreTableBindingSource
            // 
            this.genreTableBindingSource.DataMember = "GenreTable";
            this.genreTableBindingSource.DataSource = this.mLADataBaseDataSet;
            // 
            // genreTableTableAdapter
            // 
            this.genreTableTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.AlbumTableTableAdapter = this.albumTableTableAdapter;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BandTableTableAdapter = this.bandTableTableAdapter;
            this.tableAdapterManager.GenreTableTableAdapter = this.genreTableTableAdapter;
            this.tableAdapterManager.SongTableTableAdapter = this.songTableTableAdapter;
            this.tableAdapterManager.UpdateOrder = MusicLibraryApplication.MLADataBaseDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // albumTableTableAdapter
            // 
            this.albumTableTableAdapter.ClearBeforeFill = true;
            // 
            // bandTableTableAdapter
            // 
            this.bandTableTableAdapter.ClearBeforeFill = true;
            // 
            // songTableTableAdapter
            // 
            this.songTableTableAdapter.ClearBeforeFill = true;
            // 
            // genreTableDataGridView
            // 
            this.genreTableDataGridView.AllowUserToAddRows = false;
            this.genreTableDataGridView.AllowUserToDeleteRows = false;
            this.genreTableDataGridView.AutoGenerateColumns = false;
            this.genreTableDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.genreTableDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.genreTableDataGridView.DataSource = this.genreTableBindingSource;
            this.genreTableDataGridView.Location = new System.Drawing.Point(12, 108);
            this.genreTableDataGridView.Name = "genreTableDataGridView";
            this.genreTableDataGridView.ReadOnly = true;
            this.genreTableDataGridView.RowHeadersVisible = false;
            this.genreTableDataGridView.Size = new System.Drawing.Size(251, 314);
            this.genreTableDataGridView.TabIndex = 15;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "ID";
            this.dataGridViewTextBoxColumn1.HeaderText = "ID";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 25;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 25;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "GenreName";
            this.dataGridViewTextBoxColumn2.HeaderText = "Жанр";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 120;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "GenreYear";
            this.dataGridViewTextBoxColumn3.HeaderText = "Год";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 45;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 45;
            // 
            // bandTableBindingSource
            // 
            this.bandTableBindingSource.DataMember = "FK_BandTable_GenreTable";
            this.bandTableBindingSource.DataSource = this.genreTableBindingSource;
            // 
            // bandTableDataGridView
            // 
            this.bandTableDataGridView.AllowUserToAddRows = false;
            this.bandTableDataGridView.AllowUserToDeleteRows = false;
            this.bandTableDataGridView.AutoGenerateColumns = false;
            this.bandTableDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.bandTableDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12});
            this.bandTableDataGridView.DataSource = this.bandTableBindingSource;
            this.bandTableDataGridView.Location = new System.Drawing.Point(269, 108);
            this.bandTableDataGridView.Name = "bandTableDataGridView";
            this.bandTableDataGridView.ReadOnly = true;
            this.bandTableDataGridView.RowHeadersVisible = false;
            this.bandTableDataGridView.Size = new System.Drawing.Size(564, 314);
            this.bandTableDataGridView.TabIndex = 15;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "ID";
            this.dataGridViewTextBoxColumn4.HeaderText = "ID";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 25;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 25;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "GID";
            this.dataGridViewTextBoxColumn5.HeaderText = "GID";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn6.DataPropertyName = "BandName";
            this.dataGridViewTextBoxColumn6.HeaderText = "Группа";
            this.dataGridViewTextBoxColumn6.MinimumWidth = 140;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "BandYear";
            this.dataGridViewTextBoxColumn7.HeaderText = "Год";
            this.dataGridViewTextBoxColumn7.MinimumWidth = 45;
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 45;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "BandGenre";
            this.dataGridViewTextBoxColumn8.HeaderText = "Жанр";
            this.dataGridViewTextBoxColumn8.MinimumWidth = 120;
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 120;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "BandAlbumsInDB";
            this.dataGridViewTextBoxColumn9.HeaderText = "Альбомов";
            this.dataGridViewTextBoxColumn9.MinimumWidth = 70;
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 70;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "BandAvatarPath";
            this.dataGridViewTextBoxColumn10.HeaderText = "BandAvatarPath";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Visible = false;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "BandMembers";
            this.dataGridViewTextBoxColumn11.HeaderText = "BandMembers";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.Visible = false;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "BandInfo";
            this.dataGridViewTextBoxColumn12.HeaderText = "BandInfo";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.Visible = false;
            // 
            // albumTableBindingSource
            // 
            this.albumTableBindingSource.DataMember = "FK_AlbumTable_BandTable";
            this.albumTableBindingSource.DataSource = this.bandTableBindingSource;
            // 
            // albumTableDataGridView
            // 
            this.albumTableDataGridView.AllowUserToAddRows = false;
            this.albumTableDataGridView.AllowUserToDeleteRows = false;
            this.albumTableDataGridView.AutoGenerateColumns = false;
            this.albumTableDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.albumTableDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn22});
            this.albumTableDataGridView.DataSource = this.albumTableBindingSource;
            this.albumTableDataGridView.Location = new System.Drawing.Point(12, 447);
            this.albumTableDataGridView.Name = "albumTableDataGridView";
            this.albumTableDataGridView.ReadOnly = true;
            this.albumTableDataGridView.RowHeadersVisible = false;
            this.albumTableDataGridView.Size = new System.Drawing.Size(551, 283);
            this.albumTableDataGridView.TabIndex = 15;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "ID";
            this.dataGridViewTextBoxColumn13.HeaderText = "ID";
            this.dataGridViewTextBoxColumn13.MinimumWidth = 25;
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.Width = 25;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "BID";
            this.dataGridViewTextBoxColumn14.HeaderText = "BID";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.Visible = false;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "GID";
            this.dataGridViewTextBoxColumn15.HeaderText = "GID";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.Visible = false;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn16.DataPropertyName = "AlbumName";
            this.dataGridViewTextBoxColumn16.HeaderText = "Альбом";
            this.dataGridViewTextBoxColumn16.MinimumWidth = 160;
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "AlbumYear";
            this.dataGridViewTextBoxColumn17.HeaderText = "Год";
            this.dataGridViewTextBoxColumn17.MinimumWidth = 45;
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.Width = 45;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "AlbumGenre";
            this.dataGridViewTextBoxColumn18.HeaderText = "Жанр";
            this.dataGridViewTextBoxColumn18.MinimumWidth = 120;
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.Width = 120;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.DataPropertyName = "AlbumLabel";
            this.dataGridViewTextBoxColumn19.HeaderText = "Лейбл";
            this.dataGridViewTextBoxColumn19.MinimumWidth = 100;
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.DataPropertyName = "AlbumArtist";
            this.dataGridViewTextBoxColumn20.HeaderText = "AlbumArtist";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            this.dataGridViewTextBoxColumn20.Visible = false;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.DataPropertyName = "AlbumLength";
            this.dataGridViewTextBoxColumn21.HeaderText = "Длинна";
            this.dataGridViewTextBoxColumn21.MinimumWidth = 50;
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            this.dataGridViewTextBoxColumn21.Width = 50;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.DataPropertyName = "AlbumAvatarPath";
            this.dataGridViewTextBoxColumn22.HeaderText = "AlbumAvatarPath";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            this.dataGridViewTextBoxColumn22.Visible = false;
            // 
            // songTableBindingSource
            // 
            this.songTableBindingSource.DataMember = "FK_SongTable_AlbumTable";
            this.songTableBindingSource.DataSource = this.albumTableBindingSource;
            // 
            // songTableDataGridView
            // 
            this.songTableDataGridView.AllowUserToAddRows = false;
            this.songTableDataGridView.AllowUserToDeleteRows = false;
            this.songTableDataGridView.AutoGenerateColumns = false;
            this.songTableDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.songTableDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewTextBoxColumn24,
            this.dataGridViewTextBoxColumn25,
            this.dataGridViewTextBoxColumn26});
            this.songTableDataGridView.DataSource = this.songTableBindingSource;
            this.songTableDataGridView.Location = new System.Drawing.Point(569, 447);
            this.songTableDataGridView.Name = "songTableDataGridView";
            this.songTableDataGridView.ReadOnly = true;
            this.songTableDataGridView.RowHeadersVisible = false;
            this.songTableDataGridView.Size = new System.Drawing.Size(264, 283);
            this.songTableDataGridView.TabIndex = 15;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.DataPropertyName = "SongNumber";
            this.dataGridViewTextBoxColumn23.HeaderText = "№";
            this.dataGridViewTextBoxColumn23.MinimumWidth = 23;
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            this.dataGridViewTextBoxColumn23.Width = 23;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn24.DataPropertyName = "SongName";
            this.dataGridViewTextBoxColumn24.HeaderText = "Трек";
            this.dataGridViewTextBoxColumn24.MinimumWidth = 120;
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.DataPropertyName = "SongLength";
            this.dataGridViewTextBoxColumn25.HeaderText = "Длинна";
            this.dataGridViewTextBoxColumn25.MinimumWidth = 55;
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.ReadOnly = true;
            this.dataGridViewTextBoxColumn25.Width = 55;
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.DataPropertyName = "AID";
            this.dataGridViewTextBoxColumn26.HeaderText = "AID";
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.ReadOnly = true;
            this.dataGridViewTextBoxColumn26.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(12, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 19);
            this.label3.TabIndex = 16;
            this.label3.Text = "Жанры";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(265, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 19);
            this.label4.TabIndex = 17;
            this.label4.Text = "Группы";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(12, 425);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 19);
            this.label5.TabIndex = 18;
            this.label5.Text = "Альбомы";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(565, 425);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 19);
            this.label6.TabIndex = 19;
            this.label6.Text = "Композиции";
            // 
            // SearchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.ClientSize = new System.Drawing.Size(841, 742);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.songTableDataGridView);
            this.Controls.Add(this.albumTableDataGridView);
            this.Controls.Add(this.bandTableDataGridView);
            this.Controls.Add(this.genreTableDataGridView);
            this.Controls.Add(this.linkTypeComboBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SearchForm";
            this.Text = "Search";
            this.Load += new System.EventHandler(this.SearchForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mLADataBaseDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.genreTableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.genreTableDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandTableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandTableDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.albumTableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.albumTableDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.songTableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.songTableDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton genreRadioButton;
        private System.Windows.Forms.RadioButton albumRadioButton;
        private System.Windows.Forms.RadioButton bandRadioButton;
        private System.Windows.Forms.TextBox idTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox linkTypeComboBox;
        private MLADataBaseDataSet mLADataBaseDataSet;
        private System.Windows.Forms.BindingSource genreTableBindingSource;
        private MLADataBaseDataSetTableAdapters.GenreTableTableAdapter genreTableTableAdapter;
        private MLADataBaseDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private MLADataBaseDataSetTableAdapters.BandTableTableAdapter bandTableTableAdapter;
        private System.Windows.Forms.DataGridView genreTableDataGridView;
        private System.Windows.Forms.BindingSource bandTableBindingSource;
        private System.Windows.Forms.DataGridView bandTableDataGridView;
        private MLADataBaseDataSetTableAdapters.AlbumTableTableAdapter albumTableTableAdapter;
        private System.Windows.Forms.BindingSource albumTableBindingSource;
        private MLADataBaseDataSetTableAdapters.SongTableTableAdapter songTableTableAdapter;
        private System.Windows.Forms.DataGridView albumTableDataGridView;
        private System.Windows.Forms.BindingSource songTableBindingSource;
        private System.Windows.Forms.DataGridView songTableDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}