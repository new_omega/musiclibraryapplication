﻿namespace MusicLibraryApplication
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.mainFormMenuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addArtistToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addAlbumToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addGenreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataBaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateDBToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearDBToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bandShowInfoButton = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.bandsTab = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bandDeleteDataButton = new System.Windows.Forms.Button();
            this.bandGenreComboBox = new System.Windows.Forms.ComboBox();
            this.genreTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mLADataBaseDataSet = new MusicLibraryApplication.MLADataBaseDataSet();
            this.label4 = new System.Windows.Forms.Label();
            this.bandAddNewGenreButton = new System.Windows.Forms.Button();
            this.bandNameTextBox = new System.Windows.Forms.TextBox();
            this.bandNewGenreTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.bandYearTextBox = new System.Windows.Forms.TextBox();
            this.bandAddNewButton = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.bandSearchGenreComboBox = new System.Windows.Forms.ComboBox();
            this.bandSearchClearButton = new System.Windows.Forms.Button();
            this.bandSearchNameCheckBox = new System.Windows.Forms.CheckBox();
            this.bandSearchGenreCheckBox = new System.Windows.Forms.CheckBox();
            this.saveBandSearchResults = new System.Windows.Forms.Button();
            this.bandSearchYearCheckBox = new System.Windows.Forms.CheckBox();
            this.bandLastYearTextBox = new System.Windows.Forms.TextBox();
            this.bandSearchNameTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.bandFirstYearTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.bandSearchButton = new System.Windows.Forms.Button();
            this.bandsOnMainFormDGV = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.bandTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.albumsTab = new System.Windows.Forms.TabPage();
            this.albumsOnMainFormDGV = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AlbumArtist = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AlbumLength = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.albumTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.albumSearchBandComboBox = new System.Windows.Forms.ComboBox();
            this.bandTableBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.albumSearchGenreComboBox = new System.Windows.Forms.ComboBox();
            this.albumClearSearchResultsButton = new System.Windows.Forms.Button();
            this.albumSearchArtistCheckBox = new System.Windows.Forms.CheckBox();
            this.albumSaveSearchResultButton = new System.Windows.Forms.Button();
            this.albumSearchNameCheckBox = new System.Windows.Forms.CheckBox();
            this.albumSearchGenreCheckBox = new System.Windows.Forms.CheckBox();
            this.albumSearchYearCheckBox = new System.Windows.Forms.CheckBox();
            this.albumLastYearTextBox = new System.Windows.Forms.TextBox();
            this.albumSearchNameTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.albumFirstYearTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.albumSearchButton = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.albumDeleteDataButton = new System.Windows.Forms.Button();
            this.albumArtistComboBox = new System.Windows.Forms.ComboBox();
            this.albumGenreComboBox = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.albumShowInfoButton = new System.Windows.Forms.Button();
            this.albumAddNewGenreButton = new System.Windows.Forms.Button();
            this.albumNameTextBox = new System.Windows.Forms.TextBox();
            this.albumAddNewGenreTextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.albumYearTextBox = new System.Windows.Forms.TextBox();
            this.albumAddNewButton = new System.Windows.Forms.Button();
            this.genresTab = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.genreDeleteDataButton = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.genreChangeDataButton = new System.Windows.Forms.Button();
            this.genreNameTextBox = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.genreYearTextBox = new System.Windows.Forms.TextBox();
            this.genreAddNewButton = new System.Windows.Forms.Button();
            this.genresOnMainFormDGV = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bandTableTableAdapter = new MusicLibraryApplication.MLADataBaseDataSetTableAdapters.BandTableTableAdapter();
            this.tableAdapterManager = new MusicLibraryApplication.MLADataBaseDataSetTableAdapters.TableAdapterManager();
            this.genreTableTableAdapter = new MusicLibraryApplication.MLADataBaseDataSetTableAdapters.GenreTableTableAdapter();
            this.albumTableTableAdapter = new MusicLibraryApplication.MLADataBaseDataSetTableAdapters.AlbumTableTableAdapter();
            this.songBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.songTableTableAdapter = new MusicLibraryApplication.MLADataBaseDataSetTableAdapters.SongTableTableAdapter();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.mainFormMenuStrip.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.bandsTab.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.genreTableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mLADataBaseDataSet)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bandsOnMainFormDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandTableBindingSource)).BeginInit();
            this.albumsTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.albumsOnMainFormDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.albumTableBindingSource)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bandTableBindingSource1)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.genresTab.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.genresOnMainFormDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.songBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // mainFormMenuStrip
            // 
            this.mainFormMenuStrip.BackColor = System.Drawing.SystemColors.Control;
            this.mainFormMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.addToolStripMenuItem,
            this.searchToolStripMenuItem,
            this.dataBaseToolStripMenuItem});
            this.mainFormMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.mainFormMenuStrip.Name = "mainFormMenuStrip";
            this.mainFormMenuStrip.Size = new System.Drawing.Size(713, 24);
            this.mainFormMenuStrip.TabIndex = 4;
            this.mainFormMenuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(84, 20);
            this.fileToolStripMenuItem.Text = "Программа";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.exitToolStripMenuItem.Text = "Выход";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addArtistToolStripMenuItem,
            this.addAlbumToolStripMenuItem,
            this.addGenreToolStripMenuItem});
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.addToolStripMenuItem.Text = "Добавить";
            // 
            // addArtistToolStripMenuItem
            // 
            this.addArtistToolStripMenuItem.Name = "addArtistToolStripMenuItem";
            this.addArtistToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.addArtistToolStripMenuItem.Text = "Исполнителя";
            this.addArtistToolStripMenuItem.Click += new System.EventHandler(this.addArtistToolStripMenuItem_Click);
            // 
            // addAlbumToolStripMenuItem
            // 
            this.addAlbumToolStripMenuItem.Name = "addAlbumToolStripMenuItem";
            this.addAlbumToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.addAlbumToolStripMenuItem.Text = "Альбом";
            this.addAlbumToolStripMenuItem.Click += new System.EventHandler(this.addAlbumToolStripMenuItem_Click);
            // 
            // addGenreToolStripMenuItem
            // 
            this.addGenreToolStripMenuItem.Name = "addGenreToolStripMenuItem";
            this.addGenreToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.addGenreToolStripMenuItem.Text = "Жанр";
            this.addGenreToolStripMenuItem.Click += new System.EventHandler(this.addGenreToolStripMenuItem_Click);
            // 
            // searchToolStripMenuItem
            // 
            this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
            this.searchToolStripMenuItem.Size = new System.Drawing.Size(85, 20);
            this.searchToolStripMenuItem.Text = "Поиск по ID";
            this.searchToolStripMenuItem.Click += new System.EventHandler(this.searchToolStripMenuItem_Click);
            // 
            // dataBaseToolStripMenuItem
            // 
            this.dataBaseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.updateDBToolStripMenuItem,
            this.clearDBToolStripMenuItem});
            this.dataBaseToolStripMenuItem.Name = "dataBaseToolStripMenuItem";
            this.dataBaseToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.dataBaseToolStripMenuItem.Text = "База";
            // 
            // updateDBToolStripMenuItem
            // 
            this.updateDBToolStripMenuItem.Name = "updateDBToolStripMenuItem";
            this.updateDBToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.updateDBToolStripMenuItem.Text = "Обновить";
            this.updateDBToolStripMenuItem.Click += new System.EventHandler(this.updateDBToolStripMenuItem_Click);
            // 
            // clearDBToolStripMenuItem
            // 
            this.clearDBToolStripMenuItem.Name = "clearDBToolStripMenuItem";
            this.clearDBToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.clearDBToolStripMenuItem.Text = "Очистить";
            this.clearDBToolStripMenuItem.Click += new System.EventHandler(this.clearDBToolStripMenuItem_Click);
            // 
            // clearToolStripMenuItem
            // 
            this.clearToolStripMenuItem.Name = "clearToolStripMenuItem";
            this.clearToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.clearToolStripMenuItem.Text = "Очистить";
            // 
            // bandShowInfoButton
            // 
            this.bandShowInfoButton.Location = new System.Drawing.Point(3, 183);
            this.bandShowInfoButton.Name = "bandShowInfoButton";
            this.bandShowInfoButton.Size = new System.Drawing.Size(196, 43);
            this.bandShowInfoButton.TabIndex = 6;
            this.bandShowInfoButton.Text = "Показать/изменить информацию о выбранном исполнителе";
            this.bandShowInfoButton.UseVisualStyleBackColor = true;
            this.bandShowInfoButton.Click += new System.EventHandler(this.showArtistInfoButton_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.bandsTab);
            this.tabControl1.Controls.Add(this.albumsTab);
            this.tabControl1.Controls.Add(this.genresTab);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(713, 489);
            this.tabControl1.TabIndex = 7;
            // 
            // bandsTab
            // 
            this.bandsTab.AutoScroll = true;
            this.bandsTab.BackColor = System.Drawing.Color.LightBlue;
            this.bandsTab.Controls.Add(this.groupBox1);
            this.bandsTab.Controls.Add(this.groupBox2);
            this.bandsTab.Controls.Add(this.bandsOnMainFormDGV);
            this.bandsTab.Location = new System.Drawing.Point(4, 22);
            this.bandsTab.Name = "bandsTab";
            this.bandsTab.Padding = new System.Windows.Forms.Padding(3);
            this.bandsTab.Size = new System.Drawing.Size(705, 463);
            this.bandsTab.TabIndex = 0;
            this.bandsTab.Text = "Исполнители";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.bandDeleteDataButton);
            this.groupBox1.Controls.Add(this.bandGenreComboBox);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.bandShowInfoButton);
            this.groupBox1.Controls.Add(this.bandAddNewGenreButton);
            this.groupBox1.Controls.Add(this.bandNameTextBox);
            this.groupBox1.Controls.Add(this.bandNewGenreTextBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.bandYearTextBox);
            this.groupBox1.Controls.Add(this.bandAddNewButton);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(254, 233);
            this.groupBox1.TabIndex = 34;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Быстро добавить нового исполнителя в базу:";
            // 
            // bandDeleteDataButton
            // 
            this.bandDeleteDataButton.Location = new System.Drawing.Point(110, 139);
            this.bandDeleteDataButton.Name = "bandDeleteDataButton";
            this.bandDeleteDataButton.Size = new System.Drawing.Size(89, 38);
            this.bandDeleteDataButton.TabIndex = 37;
            this.bandDeleteDataButton.Text = "Удалить исполнителя";
            this.bandDeleteDataButton.UseVisualStyleBackColor = true;
            this.bandDeleteDataButton.Click += new System.EventHandler(this.deleteArtistButton_Click);
            // 
            // bandGenreComboBox
            // 
            this.bandGenreComboBox.DataSource = this.genreTableBindingSource;
            this.bandGenreComboBox.DisplayMember = "GenreName";
            this.bandGenreComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.bandGenreComboBox.FormattingEnabled = true;
            this.bandGenreComboBox.Location = new System.Drawing.Point(50, 71);
            this.bandGenreComboBox.Name = "bandGenreComboBox";
            this.bandGenreComboBox.Size = new System.Drawing.Size(198, 21);
            this.bandGenreComboBox.TabIndex = 35;
            this.bandGenreComboBox.ValueMember = "ID";
            // 
            // genreTableBindingSource
            // 
            this.genreTableBindingSource.DataMember = "GenreTable";
            this.genreTableBindingSource.DataSource = this.mLADataBaseDataSet;
            // 
            // mLADataBaseDataSet
            // 
            this.mLADataBaseDataSet.DataSetName = "MLADataBaseDataSet";
            this.mLADataBaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Имя:";
            // 
            // bandAddNewGenreButton
            // 
            this.bandAddNewGenreButton.Enabled = false;
            this.bandAddNewGenreButton.Location = new System.Drawing.Point(3, 98);
            this.bandAddNewGenreButton.Name = "bandAddNewGenreButton";
            this.bandAddNewGenreButton.Size = new System.Drawing.Size(76, 35);
            this.bandAddNewGenreButton.TabIndex = 33;
            this.bandAddNewGenreButton.Text = "Добавить новый";
            this.bandAddNewGenreButton.UseVisualStyleBackColor = true;
            this.bandAddNewGenreButton.Click += new System.EventHandler(this.addNewGenreButton_Click);
            // 
            // bandNameTextBox
            // 
            this.bandNameTextBox.Location = new System.Drawing.Point(50, 19);
            this.bandNameTextBox.Name = "bandNameTextBox";
            this.bandNameTextBox.Size = new System.Drawing.Size(198, 20);
            this.bandNameTextBox.TabIndex = 1;
            this.toolTip1.SetToolTip(this.bandNameTextBox, "Введите название группы или исполнителя");
            // 
            // bandNewGenreTextBox
            // 
            this.bandNewGenreTextBox.Location = new System.Drawing.Point(85, 106);
            this.bandNewGenreTextBox.Name = "bandNewGenreTextBox";
            this.bandNewGenreTextBox.Size = new System.Drawing.Size(163, 20);
            this.bandNewGenreTextBox.TabIndex = 31;
            this.toolTip1.SetToolTip(this.bandNewGenreTextBox, "Введите сюда название жанра, если его нет в базе");
            this.bandNewGenreTextBox.TextChanged += new System.EventHandler(this.newGenreTextBox_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Год:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(5, 74);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 13);
            this.label8.TabIndex = 30;
            this.label8.Text = "Жанр:";
            // 
            // bandYearTextBox
            // 
            this.bandYearTextBox.Location = new System.Drawing.Point(50, 45);
            this.bandYearTextBox.Name = "bandYearTextBox";
            this.bandYearTextBox.Size = new System.Drawing.Size(73, 20);
            this.bandYearTextBox.TabIndex = 2;
            this.toolTip1.SetToolTip(this.bandYearTextBox, "Год должен быть число из 4 цифр, например \"1997\"");
            this.bandYearTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.bandYearTextBox_KeyPress);
            // 
            // bandAddNewButton
            // 
            this.bandAddNewButton.Location = new System.Drawing.Point(3, 139);
            this.bandAddNewButton.Name = "bandAddNewButton";
            this.bandAddNewButton.Size = new System.Drawing.Size(89, 38);
            this.bandAddNewButton.TabIndex = 3;
            this.bandAddNewButton.Text = "Добавить исполнителя";
            this.bandAddNewButton.UseVisualStyleBackColor = true;
            this.bandAddNewButton.Click += new System.EventHandler(this.quickAddArtistButton_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.bandSearchGenreComboBox);
            this.groupBox2.Controls.Add(this.bandSearchClearButton);
            this.groupBox2.Controls.Add(this.bandSearchNameCheckBox);
            this.groupBox2.Controls.Add(this.bandSearchGenreCheckBox);
            this.groupBox2.Controls.Add(this.saveBandSearchResults);
            this.groupBox2.Controls.Add(this.bandSearchYearCheckBox);
            this.groupBox2.Controls.Add(this.bandLastYearTextBox);
            this.groupBox2.Controls.Add(this.bandSearchNameTextBox);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.bandFirstYearTextBox);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.bandSearchButton);
            this.groupBox2.Location = new System.Drawing.Point(9, 245);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(254, 180);
            this.groupBox2.TabIndex = 35;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Быстрый поиск исполнителя:";
            // 
            // bandSearchGenreComboBox
            // 
            this.bandSearchGenreComboBox.DataSource = this.genreTableBindingSource;
            this.bandSearchGenreComboBox.DisplayMember = "GenreName";
            this.bandSearchGenreComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.bandSearchGenreComboBox.Enabled = false;
            this.bandSearchGenreComboBox.FormattingEnabled = true;
            this.bandSearchGenreComboBox.Location = new System.Drawing.Point(68, 67);
            this.bandSearchGenreComboBox.Name = "bandSearchGenreComboBox";
            this.bandSearchGenreComboBox.Size = new System.Drawing.Size(180, 21);
            this.bandSearchGenreComboBox.TabIndex = 36;
            this.bandSearchGenreComboBox.ValueMember = "ID";
            // 
            // bandSearchClearButton
            // 
            this.bandSearchClearButton.Enabled = false;
            this.bandSearchClearButton.Location = new System.Drawing.Point(3, 136);
            this.bandSearchClearButton.Name = "bandSearchClearButton";
            this.bandSearchClearButton.Size = new System.Drawing.Size(115, 41);
            this.bandSearchClearButton.TabIndex = 34;
            this.bandSearchClearButton.Text = "Сбросить результаты поиска";
            this.bandSearchClearButton.UseVisualStyleBackColor = true;
            this.bandSearchClearButton.Click += new System.EventHandler(this.bandSearchClearButton_Click);
            // 
            // bandSearchNameCheckBox
            // 
            this.bandSearchNameCheckBox.AutoSize = true;
            this.bandSearchNameCheckBox.Location = new System.Drawing.Point(6, 21);
            this.bandSearchNameCheckBox.Name = "bandSearchNameCheckBox";
            this.bandSearchNameCheckBox.Size = new System.Drawing.Size(51, 17);
            this.bandSearchNameCheckBox.TabIndex = 19;
            this.bandSearchNameCheckBox.Text = "Имя:";
            this.bandSearchNameCheckBox.UseVisualStyleBackColor = true;
            this.bandSearchNameCheckBox.CheckedChanged += new System.EventHandler(this.bandNameCheckBox_CheckedChanged);
            // 
            // bandSearchGenreCheckBox
            // 
            this.bandSearchGenreCheckBox.AutoSize = true;
            this.bandSearchGenreCheckBox.Location = new System.Drawing.Point(6, 71);
            this.bandSearchGenreCheckBox.Name = "bandSearchGenreCheckBox";
            this.bandSearchGenreCheckBox.Size = new System.Drawing.Size(58, 17);
            this.bandSearchGenreCheckBox.TabIndex = 20;
            this.bandSearchGenreCheckBox.Text = "Жанр:";
            this.bandSearchGenreCheckBox.UseVisualStyleBackColor = true;
            this.bandSearchGenreCheckBox.CheckedChanged += new System.EventHandler(this.bandGenreCheckBox_CheckedChanged);
            // 
            // saveBandSearchResults
            // 
            this.saveBandSearchResults.Enabled = false;
            this.saveBandSearchResults.Location = new System.Drawing.Point(124, 136);
            this.saveBandSearchResults.Name = "saveBandSearchResults";
            this.saveBandSearchResults.Size = new System.Drawing.Size(115, 41);
            this.saveBandSearchResults.TabIndex = 29;
            this.saveBandSearchResults.Text = "Сохранить результаты поиска";
            this.saveBandSearchResults.UseVisualStyleBackColor = true;
            this.saveBandSearchResults.Click += new System.EventHandler(this.saveBandSearchResults_Click);
            // 
            // bandSearchYearCheckBox
            // 
            this.bandSearchYearCheckBox.AutoSize = true;
            this.bandSearchYearCheckBox.Location = new System.Drawing.Point(6, 45);
            this.bandSearchYearCheckBox.Name = "bandSearchYearCheckBox";
            this.bandSearchYearCheckBox.Size = new System.Drawing.Size(47, 17);
            this.bandSearchYearCheckBox.TabIndex = 21;
            this.bandSearchYearCheckBox.Text = "Год:";
            this.bandSearchYearCheckBox.UseVisualStyleBackColor = true;
            this.bandSearchYearCheckBox.CheckedChanged += new System.EventHandler(this.bandYearCheckBox_CheckedChanged);
            // 
            // bandLastYearTextBox
            // 
            this.bandLastYearTextBox.Enabled = false;
            this.bandLastYearTextBox.Location = new System.Drawing.Point(170, 43);
            this.bandLastYearTextBox.Name = "bandLastYearTextBox";
            this.bandLastYearTextBox.Size = new System.Drawing.Size(58, 20);
            this.bandLastYearTextBox.TabIndex = 28;
            this.toolTip1.SetToolTip(this.bandLastYearTextBox, "Оставьте это поле пустым, если хотите найти все группы, начиная с какого-то года");
            this.bandLastYearTextBox.Click += new System.EventHandler(this.bandLastYearTextBox_Click);
            this.bandLastYearTextBox.TextChanged += new System.EventHandler(this.SearchTextChanged);
            this.bandLastYearTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.bandLastYearTextBox_KeyPress);
            // 
            // bandSearchNameTextBox
            // 
            this.bandSearchNameTextBox.Enabled = false;
            this.bandSearchNameTextBox.Location = new System.Drawing.Point(68, 19);
            this.bandSearchNameTextBox.Name = "bandSearchNameTextBox";
            this.bandSearchNameTextBox.Size = new System.Drawing.Size(180, 20);
            this.bandSearchNameTextBox.TabIndex = 22;
            this.toolTip1.SetToolTip(this.bandSearchNameTextBox, "Введите имя группы, которую хотите найти");
            this.bandSearchNameTextBox.Click += new System.EventHandler(this.bandSearchNameTextBox_Click);
            this.bandSearchNameTextBox.TextChanged += new System.EventHandler(this.SearchTextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Enabled = false;
            this.label7.Location = new System.Drawing.Point(145, 46);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(19, 13);
            this.label7.TabIndex = 27;
            this.label7.Text = "до";
            // 
            // bandFirstYearTextBox
            // 
            this.bandFirstYearTextBox.Enabled = false;
            this.bandFirstYearTextBox.Location = new System.Drawing.Point(84, 43);
            this.bandFirstYearTextBox.Name = "bandFirstYearTextBox";
            this.bandFirstYearTextBox.Size = new System.Drawing.Size(58, 20);
            this.bandFirstYearTextBox.TabIndex = 23;
            this.toolTip1.SetToolTip(this.bandFirstYearTextBox, "Оставьте это поле пустым, если хотите найти все группы до какого-то года");
            this.bandFirstYearTextBox.Click += new System.EventHandler(this.bandFirstYearTextBox_Click);
            this.bandFirstYearTextBox.TextChanged += new System.EventHandler(this.SearchTextChanged);
            this.bandFirstYearTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.bandFirstYearTextBox_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Enabled = false;
            this.label6.Location = new System.Drawing.Point(65, 46);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 13);
            this.label6.TabIndex = 26;
            this.label6.Text = "с";
            // 
            // bandSearchButton
            // 
            this.bandSearchButton.Enabled = false;
            this.bandSearchButton.Location = new System.Drawing.Point(3, 100);
            this.bandSearchButton.Name = "bandSearchButton";
            this.bandSearchButton.Size = new System.Drawing.Size(61, 30);
            this.bandSearchButton.TabIndex = 25;
            this.bandSearchButton.Text = "Найти";
            this.bandSearchButton.UseVisualStyleBackColor = true;
            this.bandSearchButton.Click += new System.EventHandler(this.searchBandsButton_Click);
            // 
            // bandsOnMainFormDGV
            // 
            this.bandsOnMainFormDGV.AllowUserToAddRows = false;
            this.bandsOnMainFormDGV.AllowUserToDeleteRows = false;
            this.bandsOnMainFormDGV.AutoGenerateColumns = false;
            this.bandsOnMainFormDGV.BackgroundColor = System.Drawing.Color.DarkCyan;
            this.bandsOnMainFormDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.bandsOnMainFormDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6});
            this.bandsOnMainFormDGV.DataSource = this.bandTableBindingSource;
            this.bandsOnMainFormDGV.Dock = System.Windows.Forms.DockStyle.Right;
            this.bandsOnMainFormDGV.Location = new System.Drawing.Point(268, 3);
            this.bandsOnMainFormDGV.Name = "bandsOnMainFormDGV";
            this.bandsOnMainFormDGV.ReadOnly = true;
            this.bandsOnMainFormDGV.RowHeadersVisible = false;
            this.bandsOnMainFormDGV.Size = new System.Drawing.Size(434, 457);
            this.bandsOnMainFormDGV.TabIndex = 35;
            this.bandsOnMainFormDGV.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.bandsOnMainFormDGV_CellClick);
            this.bandsOnMainFormDGV.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.bandsOnMainFormDGV_CellContentDoubleClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "ID";
            this.dataGridViewTextBoxColumn1.HeaderText = "ID";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 25;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 25;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "GID";
            this.dataGridViewTextBoxColumn2.HeaderText = "GID";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Visible = false;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "BandName";
            this.dataGridViewTextBoxColumn3.HeaderText = "Имя";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 160;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "BandYear";
            this.dataGridViewTextBoxColumn4.HeaderText = "Год";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 45;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 45;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "BandGenre";
            this.dataGridViewTextBoxColumn5.HeaderText = "Жанр";
            this.dataGridViewTextBoxColumn5.MinimumWidth = 120;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 120;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "BandAlbumsInDB";
            this.dataGridViewTextBoxColumn6.HeaderText = "Альбомов";
            this.dataGridViewTextBoxColumn6.MinimumWidth = 65;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn6.Width = 65;
            // 
            // bandTableBindingSource
            // 
            this.bandTableBindingSource.DataMember = "BandTable";
            this.bandTableBindingSource.DataSource = this.mLADataBaseDataSet;
            // 
            // albumsTab
            // 
            this.albumsTab.AutoScroll = true;
            this.albumsTab.BackColor = System.Drawing.Color.LightBlue;
            this.albumsTab.Controls.Add(this.albumsOnMainFormDGV);
            this.albumsTab.Controls.Add(this.groupBox3);
            this.albumsTab.Controls.Add(this.groupBox4);
            this.albumsTab.Location = new System.Drawing.Point(4, 22);
            this.albumsTab.Name = "albumsTab";
            this.albumsTab.Padding = new System.Windows.Forms.Padding(3);
            this.albumsTab.Size = new System.Drawing.Size(705, 463);
            this.albumsTab.TabIndex = 1;
            this.albumsTab.Text = "Альбомы";
            // 
            // albumsOnMainFormDGV
            // 
            this.albumsOnMainFormDGV.AllowUserToAddRows = false;
            this.albumsOnMainFormDGV.AllowUserToDeleteRows = false;
            this.albumsOnMainFormDGV.AutoGenerateColumns = false;
            this.albumsOnMainFormDGV.BackgroundColor = System.Drawing.Color.DarkCyan;
            this.albumsOnMainFormDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.albumsOnMainFormDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.AlbumArtist,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.AlbumLength});
            this.albumsOnMainFormDGV.DataSource = this.albumTableBindingSource;
            this.albumsOnMainFormDGV.Dock = System.Windows.Forms.DockStyle.Right;
            this.albumsOnMainFormDGV.Location = new System.Drawing.Point(289, 3);
            this.albumsOnMainFormDGV.Name = "albumsOnMainFormDGV";
            this.albumsOnMainFormDGV.ReadOnly = true;
            this.albumsOnMainFormDGV.RowHeadersVisible = false;
            this.albumsOnMainFormDGV.Size = new System.Drawing.Size(413, 457);
            this.albumsOnMainFormDGV.TabIndex = 38;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "ID";
            this.dataGridViewTextBoxColumn7.HeaderText = "ID";
            this.dataGridViewTextBoxColumn7.MinimumWidth = 25;
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 25;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "BID";
            this.dataGridViewTextBoxColumn8.HeaderText = "BID";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Visible = false;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "GID";
            this.dataGridViewTextBoxColumn9.HeaderText = "GID";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Visible = false;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn10.DataPropertyName = "AlbumName";
            this.dataGridViewTextBoxColumn10.HeaderText = "Название";
            this.dataGridViewTextBoxColumn10.MinimumWidth = 140;
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            // 
            // AlbumArtist
            // 
            this.AlbumArtist.DataPropertyName = "AlbumArtist";
            this.AlbumArtist.HeaderText = "Исполнитель";
            this.AlbumArtist.MinimumWidth = 140;
            this.AlbumArtist.Name = "AlbumArtist";
            this.AlbumArtist.ReadOnly = true;
            this.AlbumArtist.Width = 140;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "AlbumYear";
            this.dataGridViewTextBoxColumn11.HeaderText = "Год";
            this.dataGridViewTextBoxColumn11.MinimumWidth = 40;
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.Width = 40;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "AlbumGenre";
            this.dataGridViewTextBoxColumn12.HeaderText = "Жанр";
            this.dataGridViewTextBoxColumn12.MinimumWidth = 65;
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.Width = 65;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "AlbumLabel";
            this.dataGridViewTextBoxColumn13.HeaderText = "AlbumLabel";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.Visible = false;
            // 
            // AlbumLength
            // 
            this.AlbumLength.DataPropertyName = "AlbumLength";
            this.AlbumLength.HeaderText = "AlbumLength";
            this.AlbumLength.Name = "AlbumLength";
            this.AlbumLength.ReadOnly = true;
            this.AlbumLength.Visible = false;
            // 
            // albumTableBindingSource
            // 
            this.albumTableBindingSource.DataMember = "AlbumTable";
            this.albumTableBindingSource.DataSource = this.mLADataBaseDataSet;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.albumSearchBandComboBox);
            this.groupBox3.Controls.Add(this.albumSearchGenreComboBox);
            this.groupBox3.Controls.Add(this.albumClearSearchResultsButton);
            this.groupBox3.Controls.Add(this.albumSearchArtistCheckBox);
            this.groupBox3.Controls.Add(this.albumSaveSearchResultButton);
            this.groupBox3.Controls.Add(this.albumSearchNameCheckBox);
            this.groupBox3.Controls.Add(this.albumSearchGenreCheckBox);
            this.groupBox3.Controls.Add(this.albumSearchYearCheckBox);
            this.groupBox3.Controls.Add(this.albumLastYearTextBox);
            this.groupBox3.Controls.Add(this.albumSearchNameTextBox);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.albumFirstYearTextBox);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.albumSearchButton);
            this.groupBox3.Location = new System.Drawing.Point(6, 272);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(275, 185);
            this.groupBox3.TabIndex = 38;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Быстрый поиск альбома:";
            // 
            // albumSearchBandComboBox
            // 
            this.albumSearchBandComboBox.DataSource = this.bandTableBindingSource1;
            this.albumSearchBandComboBox.DisplayMember = "BandName";
            this.albumSearchBandComboBox.Enabled = false;
            this.albumSearchBandComboBox.FormattingEnabled = true;
            this.albumSearchBandComboBox.Location = new System.Drawing.Point(108, 43);
            this.albumSearchBandComboBox.Name = "albumSearchBandComboBox";
            this.albumSearchBandComboBox.Size = new System.Drawing.Size(161, 21);
            this.albumSearchBandComboBox.TabIndex = 40;
            this.toolTip1.SetToolTip(this.albumSearchBandComboBox, "Введите имя исполнителя, для которого хотите найти альбомы");
            this.albumSearchBandComboBox.ValueMember = "ID";
            // 
            // bandTableBindingSource1
            // 
            this.bandTableBindingSource1.DataMember = "BandTable";
            this.bandTableBindingSource1.DataSource = this.mLADataBaseDataSet;
            // 
            // albumSearchGenreComboBox
            // 
            this.albumSearchGenreComboBox.DataSource = this.genreTableBindingSource;
            this.albumSearchGenreComboBox.DisplayMember = "GenreName";
            this.albumSearchGenreComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.albumSearchGenreComboBox.Enabled = false;
            this.albumSearchGenreComboBox.FormattingEnabled = true;
            this.albumSearchGenreComboBox.Location = new System.Drawing.Point(87, 98);
            this.albumSearchGenreComboBox.Name = "albumSearchGenreComboBox";
            this.albumSearchGenreComboBox.Size = new System.Drawing.Size(180, 21);
            this.albumSearchGenreComboBox.TabIndex = 40;
            this.albumSearchGenreComboBox.ValueMember = "ID";
            // 
            // albumClearSearchResultsButton
            // 
            this.albumClearSearchResultsButton.Enabled = false;
            this.albumClearSearchResultsButton.Location = new System.Drawing.Point(87, 154);
            this.albumClearSearchResultsButton.Name = "albumClearSearchResultsButton";
            this.albumClearSearchResultsButton.Size = new System.Drawing.Size(169, 23);
            this.albumClearSearchResultsButton.TabIndex = 39;
            this.albumClearSearchResultsButton.Text = "Сбросить результаты поиска";
            this.albumClearSearchResultsButton.UseVisualStyleBackColor = true;
            this.albumClearSearchResultsButton.Click += new System.EventHandler(this.albumClearSearchResults_Click);
            // 
            // albumSearchArtistCheckBox
            // 
            this.albumSearchArtistCheckBox.AutoSize = true;
            this.albumSearchArtistCheckBox.Location = new System.Drawing.Point(6, 47);
            this.albumSearchArtistCheckBox.Name = "albumSearchArtistCheckBox";
            this.albumSearchArtistCheckBox.Size = new System.Drawing.Size(96, 17);
            this.albumSearchArtistCheckBox.TabIndex = 37;
            this.albumSearchArtistCheckBox.Text = "Исполнитель:";
            this.albumSearchArtistCheckBox.UseVisualStyleBackColor = true;
            this.albumSearchArtistCheckBox.CheckedChanged += new System.EventHandler(this.albumSearchArtistCheckBox_CheckedChanged);
            // 
            // albumSaveSearchResultButton
            // 
            this.albumSaveSearchResultButton.Enabled = false;
            this.albumSaveSearchResultButton.Location = new System.Drawing.Point(87, 125);
            this.albumSaveSearchResultButton.Name = "albumSaveSearchResultButton";
            this.albumSaveSearchResultButton.Size = new System.Drawing.Size(169, 23);
            this.albumSaveSearchResultButton.TabIndex = 29;
            this.albumSaveSearchResultButton.Text = "Сохранить результаты поиска";
            this.albumSaveSearchResultButton.UseVisualStyleBackColor = true;
            this.albumSaveSearchResultButton.Click += new System.EventHandler(this.albumSaveSearchResultButton_Click);
            // 
            // albumSearchNameCheckBox
            // 
            this.albumSearchNameCheckBox.AutoSize = true;
            this.albumSearchNameCheckBox.Location = new System.Drawing.Point(6, 21);
            this.albumSearchNameCheckBox.Name = "albumSearchNameCheckBox";
            this.albumSearchNameCheckBox.Size = new System.Drawing.Size(79, 17);
            this.albumSearchNameCheckBox.TabIndex = 19;
            this.albumSearchNameCheckBox.Text = "Название:";
            this.albumSearchNameCheckBox.UseVisualStyleBackColor = true;
            this.albumSearchNameCheckBox.CheckedChanged += new System.EventHandler(this.albumSearchNameCheckBox_CheckedChanged);
            // 
            // albumSearchGenreCheckBox
            // 
            this.albumSearchGenreCheckBox.AutoSize = true;
            this.albumSearchGenreCheckBox.Location = new System.Drawing.Point(6, 100);
            this.albumSearchGenreCheckBox.Name = "albumSearchGenreCheckBox";
            this.albumSearchGenreCheckBox.Size = new System.Drawing.Size(58, 17);
            this.albumSearchGenreCheckBox.TabIndex = 20;
            this.albumSearchGenreCheckBox.Text = "Жанр:";
            this.albumSearchGenreCheckBox.UseVisualStyleBackColor = true;
            this.albumSearchGenreCheckBox.CheckedChanged += new System.EventHandler(this.albumSearchGenreCheckBox_CheckedChanged);
            // 
            // albumSearchYearCheckBox
            // 
            this.albumSearchYearCheckBox.AutoSize = true;
            this.albumSearchYearCheckBox.Location = new System.Drawing.Point(6, 74);
            this.albumSearchYearCheckBox.Name = "albumSearchYearCheckBox";
            this.albumSearchYearCheckBox.Size = new System.Drawing.Size(47, 17);
            this.albumSearchYearCheckBox.TabIndex = 21;
            this.albumSearchYearCheckBox.Text = "Год:";
            this.albumSearchYearCheckBox.UseVisualStyleBackColor = true;
            this.albumSearchYearCheckBox.CheckedChanged += new System.EventHandler(this.albumSearchYearCheckBox_CheckedChanged);
            // 
            // albumLastYearTextBox
            // 
            this.albumLastYearTextBox.Enabled = false;
            this.albumLastYearTextBox.Location = new System.Drawing.Point(170, 72);
            this.albumLastYearTextBox.Name = "albumLastYearTextBox";
            this.albumLastYearTextBox.Size = new System.Drawing.Size(58, 20);
            this.albumLastYearTextBox.TabIndex = 28;
            this.albumLastYearTextBox.TextChanged += new System.EventHandler(this.albumSearchTextChanged);
            this.albumLastYearTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.albumLastYear_KeyPress);
            // 
            // albumSearchNameTextBox
            // 
            this.albumSearchNameTextBox.Enabled = false;
            this.albumSearchNameTextBox.Location = new System.Drawing.Point(91, 19);
            this.albumSearchNameTextBox.Name = "albumSearchNameTextBox";
            this.albumSearchNameTextBox.Size = new System.Drawing.Size(176, 20);
            this.albumSearchNameTextBox.TabIndex = 22;
            this.toolTip1.SetToolTip(this.albumSearchNameTextBox, "Введите название альбома, который хотите найти");
            this.albumSearchNameTextBox.TextChanged += new System.EventHandler(this.albumSearchTextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Enabled = false;
            this.label1.Location = new System.Drawing.Point(145, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(19, 13);
            this.label1.TabIndex = 27;
            this.label1.Text = "до";
            // 
            // albumFirstYearTextBox
            // 
            this.albumFirstYearTextBox.Enabled = false;
            this.albumFirstYearTextBox.Location = new System.Drawing.Point(84, 72);
            this.albumFirstYearTextBox.Name = "albumFirstYearTextBox";
            this.albumFirstYearTextBox.Size = new System.Drawing.Size(58, 20);
            this.albumFirstYearTextBox.TabIndex = 23;
            this.albumFirstYearTextBox.TextChanged += new System.EventHandler(this.albumSearchTextChanged);
            this.albumFirstYearTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.albumFirstYear_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Enabled = false;
            this.label2.Location = new System.Drawing.Point(65, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 13);
            this.label2.TabIndex = 26;
            this.label2.Text = "с";
            // 
            // albumSearchButton
            // 
            this.albumSearchButton.Enabled = false;
            this.albumSearchButton.Location = new System.Drawing.Point(6, 125);
            this.albumSearchButton.Name = "albumSearchButton";
            this.albumSearchButton.Size = new System.Drawing.Size(65, 42);
            this.albumSearchButton.TabIndex = 25;
            this.albumSearchButton.Text = "Найти";
            this.albumSearchButton.UseVisualStyleBackColor = true;
            this.albumSearchButton.Click += new System.EventHandler(this.albumSearchButton_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.albumDeleteDataButton);
            this.groupBox4.Controls.Add(this.albumArtistComboBox);
            this.groupBox4.Controls.Add(this.albumGenreComboBox);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.albumShowInfoButton);
            this.groupBox4.Controls.Add(this.albumAddNewGenreButton);
            this.groupBox4.Controls.Add(this.albumNameTextBox);
            this.groupBox4.Controls.Add(this.albumAddNewGenreTextBox);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.albumYearTextBox);
            this.groupBox4.Controls.Add(this.albumAddNewButton);
            this.groupBox4.Location = new System.Drawing.Point(8, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(273, 260);
            this.groupBox4.TabIndex = 37;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Быстро добавить новый альбом в базу:";
            // 
            // albumDeleteDataButton
            // 
            this.albumDeleteDataButton.Enabled = false;
            this.albumDeleteDataButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.albumDeleteDataButton.Location = new System.Drawing.Point(123, 169);
            this.albumDeleteDataButton.Name = "albumDeleteDataButton";
            this.albumDeleteDataButton.Size = new System.Drawing.Size(82, 38);
            this.albumDeleteDataButton.TabIndex = 36;
            this.albumDeleteDataButton.Text = "Удалить альбом";
            this.albumDeleteDataButton.UseVisualStyleBackColor = true;
            this.albumDeleteDataButton.Click += new System.EventHandler(this.deleteAlbumButton_Click);
            // 
            // albumArtistComboBox
            // 
            this.albumArtistComboBox.DataSource = this.bandTableBindingSource;
            this.albumArtistComboBox.DisplayMember = "BandName";
            this.albumArtistComboBox.FormattingEnabled = true;
            this.albumArtistComboBox.Location = new System.Drawing.Point(85, 45);
            this.albumArtistComboBox.Name = "albumArtistComboBox";
            this.albumArtistComboBox.Size = new System.Drawing.Size(182, 21);
            this.albumArtistComboBox.TabIndex = 35;
            this.toolTip1.SetToolTip(this.albumArtistComboBox, "Введите или выберите имя автора(ов) альбома");
            this.albumArtistComboBox.ValueMember = "ID";
            // 
            // albumGenreComboBox
            // 
            this.albumGenreComboBox.DataSource = this.genreTableBindingSource;
            this.albumGenreComboBox.DisplayMember = "GenreName";
            this.albumGenreComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.albumGenreComboBox.FormattingEnabled = true;
            this.albumGenreComboBox.Location = new System.Drawing.Point(71, 101);
            this.albumGenreComboBox.Name = "albumGenreComboBox";
            this.albumGenreComboBox.Size = new System.Drawing.Size(196, 21);
            this.albumGenreComboBox.TabIndex = 35;
            this.albumGenreComboBox.ValueMember = "ID";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(5, 48);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 13);
            this.label11.TabIndex = 34;
            this.label11.Text = "Исполнитель:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Название:";
            // 
            // albumShowInfoButton
            // 
            this.albumShowInfoButton.Location = new System.Drawing.Point(4, 213);
            this.albumShowInfoButton.Name = "albumShowInfoButton";
            this.albumShowInfoButton.Size = new System.Drawing.Size(201, 41);
            this.albumShowInfoButton.TabIndex = 6;
            this.albumShowInfoButton.Text = "Показать/изменить информацию о выбранном альбоме";
            this.albumShowInfoButton.UseVisualStyleBackColor = true;
            this.albumShowInfoButton.Click += new System.EventHandler(this.showAlbumInfoButton_Click);
            // 
            // albumAddNewGenreButton
            // 
            this.albumAddNewGenreButton.Enabled = false;
            this.albumAddNewGenreButton.Location = new System.Drawing.Point(4, 128);
            this.albumAddNewGenreButton.Name = "albumAddNewGenreButton";
            this.albumAddNewGenreButton.Size = new System.Drawing.Size(76, 35);
            this.albumAddNewGenreButton.TabIndex = 33;
            this.albumAddNewGenreButton.Text = "Добавить новый";
            this.albumAddNewGenreButton.UseVisualStyleBackColor = true;
            this.albumAddNewGenreButton.Click += new System.EventHandler(this.albumAddNewGenreButton_Click);
            // 
            // albumNameTextBox
            // 
            this.albumNameTextBox.Location = new System.Drawing.Point(71, 19);
            this.albumNameTextBox.Name = "albumNameTextBox";
            this.albumNameTextBox.Size = new System.Drawing.Size(196, 20);
            this.albumNameTextBox.TabIndex = 7;
            this.toolTip1.SetToolTip(this.albumNameTextBox, "Введите название альбома");
            // 
            // albumAddNewGenreTextBox
            // 
            this.albumAddNewGenreTextBox.Location = new System.Drawing.Point(85, 136);
            this.albumAddNewGenreTextBox.Name = "albumAddNewGenreTextBox";
            this.albumAddNewGenreTextBox.Size = new System.Drawing.Size(182, 20);
            this.albumAddNewGenreTextBox.TabIndex = 31;
            this.toolTip1.SetToolTip(this.albumAddNewGenreTextBox, "Введите сюда название жанра, если его нет в библиотеке");
            this.albumAddNewGenreTextBox.TextChanged += new System.EventHandler(this.addNewGenreTextBox_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(5, 75);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(28, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Год:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 104);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 13);
            this.label10.TabIndex = 30;
            this.label10.Text = "Жанр:";
            // 
            // albumYearTextBox
            // 
            this.albumYearTextBox.Location = new System.Drawing.Point(71, 75);
            this.albumYearTextBox.Name = "albumYearTextBox";
            this.albumYearTextBox.Size = new System.Drawing.Size(73, 20);
            this.albumYearTextBox.TabIndex = 12;
            this.toolTip1.SetToolTip(this.albumYearTextBox, "Пожалуйста, не пытайтесь ввести ненастоящий год");
            this.albumYearTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.albumYearTextBox_KeyPress);
            // 
            // albumAddNewButton
            // 
            this.albumAddNewButton.Enabled = false;
            this.albumAddNewButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.albumAddNewButton.Location = new System.Drawing.Point(4, 169);
            this.albumAddNewButton.Name = "albumAddNewButton";
            this.albumAddNewButton.Size = new System.Drawing.Size(82, 38);
            this.albumAddNewButton.TabIndex = 14;
            this.albumAddNewButton.Text = "Добавить альбом";
            this.albumAddNewButton.UseVisualStyleBackColor = true;
            this.albumAddNewButton.Click += new System.EventHandler(this.quickAddNewAlbumButton_Click);
            // 
            // genresTab
            // 
            this.genresTab.AutoScroll = true;
            this.genresTab.BackColor = System.Drawing.Color.LightBlue;
            this.genresTab.Controls.Add(this.groupBox5);
            this.genresTab.Controls.Add(this.genresOnMainFormDGV);
            this.genresTab.Location = new System.Drawing.Point(4, 22);
            this.genresTab.Name = "genresTab";
            this.genresTab.Size = new System.Drawing.Size(705, 463);
            this.genresTab.TabIndex = 2;
            this.genresTab.Text = "Жанры";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.genreDeleteDataButton);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Controls.Add(this.genreChangeDataButton);
            this.groupBox5.Controls.Add(this.genreNameTextBox);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.genreYearTextBox);
            this.groupBox5.Controls.Add(this.genreAddNewButton);
            this.groupBox5.Location = new System.Drawing.Point(8, 16);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(328, 132);
            this.groupBox5.TabIndex = 35;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Добавить новый жанр в библиотеку:";
            // 
            // genreDeleteDataButton
            // 
            this.genreDeleteDataButton.Enabled = false;
            this.genreDeleteDataButton.Location = new System.Drawing.Point(220, 71);
            this.genreDeleteDataButton.Name = "genreDeleteDataButton";
            this.genreDeleteDataButton.Size = new System.Drawing.Size(101, 53);
            this.genreDeleteDataButton.TabIndex = 37;
            this.genreDeleteDataButton.Text = "Удалить жанр из библиотеки";
            this.genreDeleteDataButton.UseVisualStyleBackColor = true;
            this.genreDeleteDataButton.Click += new System.EventHandler(this.genreDeleteDataButton_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(5, 22);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(60, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "Название:";
            // 
            // genreChangeDataButton
            // 
            this.genreChangeDataButton.Enabled = false;
            this.genreChangeDataButton.Location = new System.Drawing.Point(113, 71);
            this.genreChangeDataButton.Name = "genreChangeDataButton";
            this.genreChangeDataButton.Size = new System.Drawing.Size(101, 53);
            this.genreChangeDataButton.TabIndex = 6;
            this.genreChangeDataButton.Text = "Изменить информацию о жанре";
            this.genreChangeDataButton.UseVisualStyleBackColor = true;
            this.genreChangeDataButton.Click += new System.EventHandler(this.genreChangeDataButton_Click);
            // 
            // genreNameTextBox
            // 
            this.genreNameTextBox.Location = new System.Drawing.Point(71, 19);
            this.genreNameTextBox.Name = "genreNameTextBox";
            this.genreNameTextBox.Size = new System.Drawing.Size(198, 20);
            this.genreNameTextBox.TabIndex = 7;
            this.toolTip1.SetToolTip(this.genreNameTextBox, "Введите название жанра");
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(5, 48);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(97, 13);
            this.label13.TabIndex = 10;
            this.label13.Text = "Год образования:";
            // 
            // genreYearTextBox
            // 
            this.genreYearTextBox.Location = new System.Drawing.Point(108, 45);
            this.genreYearTextBox.Name = "genreYearTextBox";
            this.genreYearTextBox.Size = new System.Drawing.Size(73, 20);
            this.genreYearTextBox.TabIndex = 12;
            this.toolTip1.SetToolTip(this.genreYearTextBox, "Введите примерный год появления жанра (необязательное поле)");
            this.genreYearTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.genreYearTextBox_KeyPress);
            // 
            // genreAddNewButton
            // 
            this.genreAddNewButton.Location = new System.Drawing.Point(8, 71);
            this.genreAddNewButton.Name = "genreAddNewButton";
            this.genreAddNewButton.Size = new System.Drawing.Size(99, 53);
            this.genreAddNewButton.TabIndex = 14;
            this.genreAddNewButton.Text = "Добавить новый жанр";
            this.genreAddNewButton.UseVisualStyleBackColor = true;
            this.genreAddNewButton.Click += new System.EventHandler(this.genreAddNewButton_Click);
            // 
            // genresOnMainFormDGV
            // 
            this.genresOnMainFormDGV.AllowUserToAddRows = false;
            this.genresOnMainFormDGV.AllowUserToDeleteRows = false;
            this.genresOnMainFormDGV.AutoGenerateColumns = false;
            this.genresOnMainFormDGV.BackgroundColor = System.Drawing.Color.DarkCyan;
            this.genresOnMainFormDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.genresOnMainFormDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16});
            this.genresOnMainFormDGV.DataSource = this.genreTableBindingSource;
            this.genresOnMainFormDGV.Dock = System.Windows.Forms.DockStyle.Right;
            this.genresOnMainFormDGV.Location = new System.Drawing.Point(344, 0);
            this.genresOnMainFormDGV.Name = "genresOnMainFormDGV";
            this.genresOnMainFormDGV.ReadOnly = true;
            this.genresOnMainFormDGV.RowHeadersVisible = false;
            this.genresOnMainFormDGV.Size = new System.Drawing.Size(361, 463);
            this.genresOnMainFormDGV.TabIndex = 0;
            this.genresOnMainFormDGV.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.genresOnMainFormDGV_CellClick);
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "ID";
            this.dataGridViewTextBoxColumn14.HeaderText = "ID";
            this.dataGridViewTextBoxColumn14.MinimumWidth = 25;
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.Width = 25;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn15.DataPropertyName = "GenreName";
            this.dataGridViewTextBoxColumn15.HeaderText = "Название";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "GenreYear";
            this.dataGridViewTextBoxColumn16.HeaderText = "Год";
            this.dataGridViewTextBoxColumn16.MinimumWidth = 55;
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            this.dataGridViewTextBoxColumn16.Width = 55;
            // 
            // bandTableTableAdapter
            // 
            this.bandTableTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.AlbumTableTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BandTableTableAdapter = this.bandTableTableAdapter;
            this.tableAdapterManager.GenreTableTableAdapter = this.genreTableTableAdapter;
            this.tableAdapterManager.SongTableTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = MusicLibraryApplication.MLADataBaseDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // genreTableTableAdapter
            // 
            this.genreTableTableAdapter.ClearBeforeFill = true;
            // 
            // albumTableTableAdapter
            // 
            this.albumTableTableAdapter.ClearBeforeFill = true;
            // 
            // songBindingSource
            // 
            this.songBindingSource.DataMember = "SongTable";
            this.songBindingSource.DataSource = this.mLADataBaseDataSet;
            // 
            // songTableTableAdapter
            // 
            this.songTableTableAdapter.ClearBeforeFill = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(713, 513);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.mainFormMenuStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.mainFormMenuStrip;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Text = "Music Library (ver 0.8)";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.exitToolStripMenuItem_Click);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.mainFormMenuStrip.ResumeLayout(false);
            this.mainFormMenuStrip.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.bandsTab.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.genreTableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mLADataBaseDataSet)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bandsOnMainFormDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandTableBindingSource)).EndInit();
            this.albumsTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.albumsOnMainFormDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.albumTableBindingSource)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bandTableBindingSource1)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.genresTab.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.genresOnMainFormDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.songBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mainFormMenuStrip;
        private System.Windows.Forms.Button bandShowInfoButton;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage bandsTab;
        private System.Windows.Forms.TabPage albumsTab;
        private System.Windows.Forms.TabPage genresTab;
        private System.Windows.Forms.Button saveBandSearchResults;
        private System.Windows.Forms.TextBox bandLastYearTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button bandSearchButton;
        private System.Windows.Forms.TextBox bandFirstYearTextBox;
        private System.Windows.Forms.TextBox bandSearchNameTextBox;
        private System.Windows.Forms.CheckBox bandSearchYearCheckBox;
        private System.Windows.Forms.CheckBox bandSearchGenreCheckBox;
        private System.Windows.Forms.CheckBox bandSearchNameCheckBox;
        private System.Windows.Forms.Button bandAddNewButton;
        private System.Windows.Forms.TextBox bandYearTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox bandNameTextBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button bandAddNewGenreButton;
        private System.Windows.Forms.TextBox bandNewGenreTextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox albumSearchArtistCheckBox;
        private System.Windows.Forms.CheckBox albumSearchNameCheckBox;
        private System.Windows.Forms.CheckBox albumSearchGenreCheckBox;
        private System.Windows.Forms.Button albumSaveSearchResultButton;
        private System.Windows.Forms.CheckBox albumSearchYearCheckBox;
        private System.Windows.Forms.TextBox albumLastYearTextBox;
        private System.Windows.Forms.TextBox albumSearchNameTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox albumFirstYearTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button albumSearchButton;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button albumShowInfoButton;
        private System.Windows.Forms.Button albumAddNewGenreButton;
        private System.Windows.Forms.TextBox albumNameTextBox;
        private System.Windows.Forms.TextBox albumAddNewGenreTextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox albumYearTextBox;
        private System.Windows.Forms.Button albumAddNewButton;
        private System.Windows.Forms.Button bandSearchClearButton;
        private MLADataBaseDataSet mLADataBaseDataSet;
        private System.Windows.Forms.BindingSource bandTableBindingSource;
        private MLADataBaseDataSetTableAdapters.BandTableTableAdapter bandTableTableAdapter;
        private MLADataBaseDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView bandsOnMainFormDGV;
        private MLADataBaseDataSetTableAdapters.GenreTableTableAdapter genreTableTableAdapter;
        private System.Windows.Forms.BindingSource genreTableBindingSource;
        private System.Windows.Forms.ComboBox bandGenreComboBox;
        private System.Windows.Forms.ComboBox bandSearchGenreComboBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewButtonColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.BindingSource albumTableBindingSource;
        private MLADataBaseDataSetTableAdapters.AlbumTableTableAdapter albumTableTableAdapter;
        private System.Windows.Forms.DataGridView albumsOnMainFormDGV;
        private System.Windows.Forms.ComboBox albumArtistComboBox;
        private System.Windows.Forms.ComboBox albumGenreComboBox;
        private System.Windows.Forms.Button albumClearSearchResultsButton;
        private System.Windows.Forms.Button bandDeleteDataButton;
        private System.Windows.Forms.Button albumDeleteDataButton;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button genreDeleteDataButton;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button genreChangeDataButton;
        private System.Windows.Forms.TextBox genreNameTextBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox genreYearTextBox;
        private System.Windows.Forms.Button genreAddNewButton;
        private System.Windows.Forms.DataGridView genresOnMainFormDGV;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.ToolStripMenuItem clearToolStripMenuItem;
        private System.Windows.Forms.BindingSource songBindingSource;
        private MLADataBaseDataSetTableAdapters.SongTableTableAdapter songTableTableAdapter;
        private System.Windows.Forms.ComboBox albumSearchGenreComboBox;
        private System.Windows.Forms.ComboBox albumSearchBandComboBox;
        private System.Windows.Forms.BindingSource bandTableBindingSource1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn AlbumArtist;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn AlbumLength;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addArtistToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addAlbumToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addGenreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataBaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateDBToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearDBToolStripMenuItem;
    }
}

