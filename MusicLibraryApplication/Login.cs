﻿using System;
using System.Windows.Forms;

namespace MusicLibraryApplication
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }
        
        public void EnterNextForm()
        {
            MainForm mn = new MainForm();
            mn.Show();
            this.Visible = false;
            this.ShowInTaskbar = false;
        }

        private void buttonEnter_Click(object sender, EventArgs e)
        {
            if (loginTextBox.Text == "" && passwordTextBox.Text == "")
            {
                MessageBox.Show("Введите данные для авторизации!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            if (loginTextBox.Text == "admin" && passwordTextBox.Text == "password")
            {
                MessageBox.Show("Вы зашли как администратор!", "Авторизация завершена",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                Options.ifAdmin = true;
                EnterNextForm();
            }
            else
            {
                MessageBox.Show("Неправильный логин или пароль!",
                    "Авторизация не завершена", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Logon_KeyDown(object sender, KeyEventArgs e)
        {
            if (loginTextBox.Text == "" && passwordTextBox.Text == "") return;
            
            if (e.KeyCode == Keys.Enter)
            {
                buttonEnter.PerformClick();
            }
        }

        private void passwordTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (loginTextBox.Text == "" && passwordTextBox.Text == "")
                return;
            if (e.KeyCode == Keys.Enter)
            {
                buttonEnter.PerformClick();
            }
        }

        private void loginTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (loginTextBox.Text == "" && passwordTextBox.Text == "") return;
            if (e.KeyCode == Keys.Enter)
            {
                buttonEnter.PerformClick();
            }
        }

        private void loginAsGuest_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Вы зашли как пользователь!", "Авторизация завершена",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            Options.ifAdmin = false;
            EnterNextForm();
        }
    }

}
