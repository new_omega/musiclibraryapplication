﻿namespace MusicLibraryApplication
{
    partial class AlbumInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AlbumInfo));
            this.saveAlbumButton = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.songsDGV = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.songTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mLADataBaseDataSet = new MusicLibraryApplication.MLADataBaseDataSet();
            this.songLengthTextBox = new System.Windows.Forms.MaskedTextBox();
            this.deleteSongButton = new System.Windows.Forms.Button();
            this.scanFolderButton = new System.Windows.Forms.Button();
            this.addSongButton = new System.Windows.Forms.Button();
            this.songNameTextBox = new System.Windows.Forms.TextBox();
            this.albumTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.chooseCoverButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.deleteCoverButton = new System.Windows.Forms.Button();
            this.albumNameTextBox = new System.Windows.Forms.TextBox();
            this.albumIDlabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.coverPictureBox = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.albumLabelTextBox = new System.Windows.Forms.TextBox();
            this.albumGenreComboBox = new System.Windows.Forms.ComboBox();
            this.genreTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.albumBandComboBox = new System.Windows.Forms.ComboBox();
            this.bandTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.albumLengthMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.albumYearTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.bandTableTableAdapter = new MusicLibraryApplication.MLADataBaseDataSetTableAdapters.BandTableTableAdapter();
            this.tableAdapterManager = new MusicLibraryApplication.MLADataBaseDataSetTableAdapters.TableAdapterManager();
            this.genreTableTableAdapter = new MusicLibraryApplication.MLADataBaseDataSetTableAdapters.GenreTableTableAdapter();
            this.deleteAlbumButton = new System.Windows.Forms.Button();
            this.albumTableTableAdapter = new MusicLibraryApplication.MLADataBaseDataSetTableAdapters.AlbumTableTableAdapter();
            this.songTableTableAdapter = new MusicLibraryApplication.MLADataBaseDataSetTableAdapters.SongTableTableAdapter();
            this.closeFormButton = new System.Windows.Forms.Button();
            this.albumToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.songsDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.songTableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mLADataBaseDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.albumTableBindingSource)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.coverPictureBox)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.genreTableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandTableBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // saveAlbumButton
            // 
            this.saveAlbumButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.saveAlbumButton.Location = new System.Drawing.Point(311, 428);
            this.saveAlbumButton.Name = "saveAlbumButton";
            this.saveAlbumButton.Size = new System.Drawing.Size(114, 42);
            this.saveAlbumButton.TabIndex = 30;
            this.saveAlbumButton.Text = "Сохранить данные об альбоме";
            this.saveAlbumButton.UseVisualStyleBackColor = true;
            this.saveAlbumButton.Click += new System.EventHandler(this.saveAlbumButton_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.songsDGV);
            this.groupBox3.Controls.Add(this.songLengthTextBox);
            this.groupBox3.Controls.Add(this.deleteSongButton);
            this.groupBox3.Controls.Add(this.scanFolderButton);
            this.groupBox3.Controls.Add(this.addSongButton);
            this.groupBox3.Controls.Add(this.songNameTextBox);
            this.groupBox3.Location = new System.Drawing.Point(305, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(357, 402);
            this.groupBox3.TabIndex = 29;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Список песен";
            // 
            // songsDGV
            // 
            this.songsDGV.AllowUserToAddRows = false;
            this.songsDGV.AllowUserToDeleteRows = false;
            this.songsDGV.AutoGenerateColumns = false;
            this.songsDGV.BackgroundColor = System.Drawing.Color.DarkCyan;
            this.songsDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.songsDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            this.songsDGV.DataSource = this.songTableBindingSource;
            this.songsDGV.Location = new System.Drawing.Point(6, 16);
            this.songsDGV.Name = "songsDGV";
            this.songsDGV.ReadOnly = true;
            this.songsDGV.RowHeadersVisible = false;
            this.songsDGV.Size = new System.Drawing.Size(346, 322);
            this.songsDGV.TabIndex = 32;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "SongNumber";
            this.dataGridViewTextBoxColumn1.HeaderText = "№";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 25;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 25;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "SongName";
            this.dataGridViewTextBoxColumn2.HeaderText = "Название";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 120;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "SongLength";
            this.dataGridViewTextBoxColumn3.HeaderText = "Длительность";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 85;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 85;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "AID";
            this.dataGridViewTextBoxColumn4.HeaderText = "AID";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // songTableBindingSource
            // 
            this.songTableBindingSource.DataMember = "SongTable";
            this.songTableBindingSource.DataSource = this.mLADataBaseDataSet;
            // 
            // mLADataBaseDataSet
            // 
            this.mLADataBaseDataSet.DataSetName = "MLADataBaseDataSet";
            this.mLADataBaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // songLengthTextBox
            // 
            this.songLengthTextBox.Location = new System.Drawing.Point(305, 345);
            this.songLengthTextBox.Mask = "00:00";
            this.songLengthTextBox.Name = "songLengthTextBox";
            this.songLengthTextBox.Size = new System.Drawing.Size(47, 20);
            this.songLengthTextBox.TabIndex = 32;
            this.albumToolTip.SetToolTip(this.songLengthTextBox, "Длительность песни в формате mm:ss");
            this.songLengthTextBox.ValidatingType = typeof(System.DateTime);
            // 
            // deleteSongButton
            // 
            this.deleteSongButton.Enabled = false;
            this.deleteSongButton.Location = new System.Drawing.Point(6, 373);
            this.deleteSongButton.Name = "deleteSongButton";
            this.deleteSongButton.Size = new System.Drawing.Size(75, 23);
            this.deleteSongButton.TabIndex = 23;
            this.deleteSongButton.Text = "Удалить";
            this.deleteSongButton.UseVisualStyleBackColor = true;
            this.deleteSongButton.Click += new System.EventHandler(this.deleteSongButton_Click);
            // 
            // scanFolderButton
            // 
            this.scanFolderButton.Location = new System.Drawing.Point(113, 370);
            this.scanFolderButton.Name = "scanFolderButton";
            this.scanFolderButton.Size = new System.Drawing.Size(238, 29);
            this.scanFolderButton.TabIndex = 22;
            this.scanFolderButton.Text = "Просканировать папку и добавить песни";
            this.scanFolderButton.UseVisualStyleBackColor = true;
            this.scanFolderButton.Click += new System.EventHandler(this.scanFolderButton_Click);
            // 
            // addSongButton
            // 
            this.addSongButton.Location = new System.Drawing.Point(6, 342);
            this.addSongButton.Name = "addSongButton";
            this.addSongButton.Size = new System.Drawing.Size(75, 23);
            this.addSongButton.TabIndex = 19;
            this.addSongButton.Text = "Добавить";
            this.addSongButton.UseVisualStyleBackColor = true;
            this.addSongButton.Click += new System.EventHandler(this.addSongButton_Click);
            // 
            // songNameTextBox
            // 
            this.songNameTextBox.Location = new System.Drawing.Point(87, 344);
            this.songNameTextBox.Name = "songNameTextBox";
            this.songNameTextBox.Size = new System.Drawing.Size(212, 20);
            this.songNameTextBox.TabIndex = 20;
            this.songNameTextBox.Text = "Введите название песни";
            this.albumToolTip.SetToolTip(this.songNameTextBox, "Название песни в альбоме");
            this.songNameTextBox.Click += new System.EventHandler(this.songNameTextBox_TextChanged);
            this.songNameTextBox.TextChanged += new System.EventHandler(this.songNameTextBox_TextChanged);
            // 
            // albumTableBindingSource
            // 
            this.albumTableBindingSource.DataMember = "AlbumTable";
            this.albumTableBindingSource.DataSource = this.mLADataBaseDataSet;
            // 
            // chooseCoverButton
            // 
            this.chooseCoverButton.Enabled = false;
            this.chooseCoverButton.Location = new System.Drawing.Point(6, 299);
            this.chooseCoverButton.Name = "chooseCoverButton";
            this.chooseCoverButton.Size = new System.Drawing.Size(75, 23);
            this.chooseCoverButton.TabIndex = 27;
            this.chooseCoverButton.Text = "Обзор...";
            this.chooseCoverButton.UseVisualStyleBackColor = true;
            this.chooseCoverButton.Click += new System.EventHandler(this.chooseCoverTextBox_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.deleteCoverButton);
            this.groupBox1.Controls.Add(this.albumNameTextBox);
            this.groupBox1.Controls.Add(this.albumIDlabel);
            this.groupBox1.Controls.Add(this.chooseCoverButton);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.coverPictureBox);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(287, 327);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Информация о записи";
            // 
            // deleteCoverButton
            // 
            this.deleteCoverButton.Location = new System.Drawing.Point(138, 299);
            this.deleteCoverButton.Name = "deleteCoverButton";
            this.deleteCoverButton.Size = new System.Drawing.Size(118, 23);
            this.deleteCoverButton.TabIndex = 28;
            this.deleteCoverButton.Text = "Удалить обложку";
            this.deleteCoverButton.UseVisualStyleBackColor = true;
            this.deleteCoverButton.Click += new System.EventHandler(this.deleteCoverButton_Click);
            // 
            // albumNameTextBox
            // 
            this.albumNameTextBox.Location = new System.Drawing.Point(61, 16);
            this.albumNameTextBox.Name = "albumNameTextBox";
            this.albumNameTextBox.Size = new System.Drawing.Size(168, 20);
            this.albumNameTextBox.TabIndex = 9;
            this.albumToolTip.SetToolTip(this.albumNameTextBox, "Название альбома");
            this.albumNameTextBox.TextChanged += new System.EventHandler(this.albumNameTextBox_TextChanged);
            // 
            // albumIDlabel
            // 
            this.albumIDlabel.AutoSize = true;
            this.albumIDlabel.Location = new System.Drawing.Point(255, 19);
            this.albumIDlabel.Name = "albumIDlabel";
            this.albumIDlabel.Size = new System.Drawing.Size(42, 13);
            this.albumIDlabel.TabIndex = 8;
            this.albumIDlabel.Text = "id_here";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Альбом:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(235, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "ID:";
            // 
            // coverPictureBox
            // 
            this.coverPictureBox.Location = new System.Drawing.Point(6, 43);
            this.coverPictureBox.Name = "coverPictureBox";
            this.coverPictureBox.Size = new System.Drawing.Size(250, 250);
            this.coverPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.coverPictureBox.TabIndex = 25;
            this.coverPictureBox.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.albumLabelTextBox);
            this.groupBox2.Controls.Add(this.albumGenreComboBox);
            this.groupBox2.Controls.Add(this.albumBandComboBox);
            this.groupBox2.Controls.Add(this.albumLengthMaskedTextBox);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.albumYearTextBox);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Location = new System.Drawing.Point(12, 345);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(287, 125);
            this.groupBox2.TabIndex = 28;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Информация об альбоме";
            // 
            // albumLabelTextBox
            // 
            this.albumLabelTextBox.Location = new System.Drawing.Point(179, 97);
            this.albumLabelTextBox.Name = "albumLabelTextBox";
            this.albumLabelTextBox.Size = new System.Drawing.Size(98, 20);
            this.albumLabelTextBox.TabIndex = 33;
            this.albumToolTip.SetToolTip(this.albumLabelTextBox, "Укажите, на каком лейбле вышел этот альбом (необязательное поле)");
            // 
            // albumGenreComboBox
            // 
            this.albumGenreComboBox.DataSource = this.genreTableBindingSource;
            this.albumGenreComboBox.DisplayMember = "GenreName";
            this.albumGenreComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.albumGenreComboBox.FormattingEnabled = true;
            this.albumGenreComboBox.Location = new System.Drawing.Point(89, 71);
            this.albumGenreComboBox.Name = "albumGenreComboBox";
            this.albumGenreComboBox.Size = new System.Drawing.Size(188, 21);
            this.albumGenreComboBox.TabIndex = 31;
            this.albumGenreComboBox.ValueMember = "ID";
            // 
            // genreTableBindingSource
            // 
            this.genreTableBindingSource.DataMember = "GenreTable";
            this.genreTableBindingSource.DataSource = this.mLADataBaseDataSet;
            // 
            // albumBandComboBox
            // 
            this.albumBandComboBox.DataSource = this.bandTableBindingSource;
            this.albumBandComboBox.DisplayMember = "BandName";
            this.albumBandComboBox.FormattingEnabled = true;
            this.albumBandComboBox.Location = new System.Drawing.Point(89, 18);
            this.albumBandComboBox.Name = "albumBandComboBox";
            this.albumBandComboBox.Size = new System.Drawing.Size(188, 21);
            this.albumBandComboBox.TabIndex = 25;
            this.albumBandComboBox.ValueMember = "ID";
            // 
            // bandTableBindingSource
            // 
            this.bandTableBindingSource.DataMember = "BandTable";
            this.bandTableBindingSource.DataSource = this.mLADataBaseDataSet;
            // 
            // albumLengthMaskedTextBox
            // 
            this.albumLengthMaskedTextBox.Location = new System.Drawing.Point(89, 97);
            this.albumLengthMaskedTextBox.Mask = "00:00";
            this.albumLengthMaskedTextBox.Name = "albumLengthMaskedTextBox";
            this.albumLengthMaskedTextBox.Size = new System.Drawing.Size(36, 20);
            this.albumLengthMaskedTextBox.TabIndex = 31;
            this.albumToolTip.SetToolTip(this.albumLengthMaskedTextBox, "Длительнолсть альбома в формате mm:ss");
            this.albumLengthMaskedTextBox.ValidatingType = typeof(System.DateTime);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Исполнитель:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Жанр:";
            // 
            // albumYearTextBox
            // 
            this.albumYearTextBox.Location = new System.Drawing.Point(89, 45);
            this.albumYearTextBox.Name = "albumYearTextBox";
            this.albumYearTextBox.Size = new System.Drawing.Size(50, 20);
            this.albumYearTextBox.TabIndex = 13;
            this.albumToolTip.SetToolTip(this.albumYearTextBox, "Год выхода этого альбома");
            this.albumYearTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.albumYearTextBox_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Год выхода:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 100);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Длительность: ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(131, 100);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 13);
            this.label7.TabIndex = 32;
            this.label7.Text = "Лейбл:";
            // 
            // bandTableTableAdapter
            // 
            this.bandTableTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.AlbumTableTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BandTableTableAdapter = this.bandTableTableAdapter;
            this.tableAdapterManager.GenreTableTableAdapter = this.genreTableTableAdapter;
            this.tableAdapterManager.SongTableTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = MusicLibraryApplication.MLADataBaseDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // genreTableTableAdapter
            // 
            this.genreTableTableAdapter.ClearBeforeFill = true;
            // 
            // deleteAlbumButton
            // 
            this.deleteAlbumButton.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.deleteAlbumButton.Location = new System.Drawing.Point(431, 428);
            this.deleteAlbumButton.Name = "deleteAlbumButton";
            this.deleteAlbumButton.Size = new System.Drawing.Size(111, 42);
            this.deleteAlbumButton.TabIndex = 31;
            this.deleteAlbumButton.Text = "Удалить данные об альбоме";
            this.deleteAlbumButton.UseVisualStyleBackColor = true;
            this.deleteAlbumButton.Click += new System.EventHandler(this.deleteAlbumButton_Click);
            // 
            // albumTableTableAdapter
            // 
            this.albumTableTableAdapter.ClearBeforeFill = true;
            // 
            // songTableTableAdapter
            // 
            this.songTableTableAdapter.ClearBeforeFill = true;
            // 
            // closeFormButton
            // 
            this.closeFormButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.closeFormButton.Location = new System.Drawing.Point(584, 428);
            this.closeFormButton.Name = "closeFormButton";
            this.closeFormButton.Size = new System.Drawing.Size(73, 42);
            this.closeFormButton.TabIndex = 32;
            this.closeFormButton.Text = "Выход";
            this.albumToolTip.SetToolTip(this.closeFormButton, "Выход из редактора без сохранения изменений");
            this.closeFormButton.UseVisualStyleBackColor = true;
            // 
            // AlbumInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.ClientSize = new System.Drawing.Size(671, 478);
            this.Controls.Add(this.closeFormButton);
            this.Controls.Add(this.deleteAlbumButton);
            this.Controls.Add(this.saveAlbumButton);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AlbumInfo";
            this.Text = "Редактор альбома";
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.songsDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.songTableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mLADataBaseDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.albumTableBindingSource)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.coverPictureBox)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.genreTableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandTableBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button saveAlbumButton;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button deleteSongButton;
        private System.Windows.Forms.Button scanFolderButton;
        private System.Windows.Forms.Button addSongButton;
        private System.Windows.Forms.TextBox songNameTextBox;
        private System.Windows.Forms.Button chooseCoverButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox albumNameTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox coverPictureBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox albumYearTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.MaskedTextBox albumLengthMaskedTextBox;
        private System.Windows.Forms.MaskedTextBox songLengthTextBox;
        private MLADataBaseDataSet mLADataBaseDataSet;
        private System.Windows.Forms.BindingSource bandTableBindingSource;
        private MLADataBaseDataSetTableAdapters.BandTableTableAdapter bandTableTableAdapter;
        private MLADataBaseDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.ComboBox albumBandComboBox;
        private MLADataBaseDataSetTableAdapters.GenreTableTableAdapter genreTableTableAdapter;
        private System.Windows.Forms.BindingSource genreTableBindingSource;
        private System.Windows.Forms.ComboBox albumGenreComboBox;
        private System.Windows.Forms.Button deleteAlbumButton;
        private System.Windows.Forms.BindingSource albumTableBindingSource;
        private MLADataBaseDataSetTableAdapters.AlbumTableTableAdapter albumTableTableAdapter;
        private System.Windows.Forms.TextBox albumLabelTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.BindingSource songTableBindingSource;
        private MLADataBaseDataSetTableAdapters.SongTableTableAdapter songTableTableAdapter;
        private System.Windows.Forms.DataGridView songsDGV;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.Button deleteCoverButton;
        public System.Windows.Forms.Label albumIDlabel;
        private System.Windows.Forms.Button closeFormButton;
        private System.Windows.Forms.ToolTip albumToolTip;
    }
}