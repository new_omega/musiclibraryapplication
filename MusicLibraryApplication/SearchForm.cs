﻿using System;
using System.Windows.Forms;

namespace MusicLibraryApplication
{
    public partial class SearchForm : Form
    {
        public SearchForm()
        {
            InitializeComponent();
        }

        private void SearchForm_Load(object sender, EventArgs e)
        {            
            this.songTableTableAdapter.Fill(this.mLADataBaseDataSet.SongTable);            
            this.albumTableTableAdapter.Fill(this.mLADataBaseDataSet.AlbumTable);            
            this.bandTableTableAdapter.Fill(this.mLADataBaseDataSet.BandTable);
            this.genreTableTableAdapter.Fill(this.mLADataBaseDataSet.GenreTable);
        }

        private void genreRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (!searchButton.Enabled && idTextBox.Text != "") searchButton.Enabled = true;
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            bool found = false;
            if (genreRadioButton.Checked) 
            {
                for (int i = 0; i < genreTableDataGridView.RowCount; i++)
                {
                    if (genreTableDataGridView.Rows[i].Cells[0].Value.ToString() == idTextBox.Text)
                    {
                        genreTableDataGridView.Rows[i].Selected = true;
                        found = true; 
                        return;
                    }
                }
            }
            else if (bandRadioButton.Checked)
            {
                for (int i = 0; i < bandTableDataGridView.RowCount; i++)
                {
                    if (bandTableDataGridView.Rows[i].Cells[0].Value.ToString() == idTextBox.Text)
                    {
                        bandTableDataGridView.Rows[i].Selected = true;
                        found = true;
                        return;
                    }
                }
            }
            else
                for (int i = 0; i < albumTableDataGridView.RowCount; i++)
                {
                    if (albumTableDataGridView.Rows[i].Cells[0].Value.ToString() == idTextBox.Text)
                    {
                        albumTableDataGridView.Rows[i].Selected = true;
                        found = true;
                        return;
                    }
                }
            if (!found) MessageBox.Show("Элемент с таким ID в базе не найден.", 
                "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void idTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != 8 && (e.KeyChar < 48 || e.KeyChar > 57))
                e.Handled = true;
        }

        private void linkTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (linkTypeComboBox.SelectedIndex == 1)
            {
                this.albumTableBindingSource.DataSource = this.genreTableBindingSource;
                this.albumTableBindingSource.DataMember = "FK_AlbumTable_GenreTable";
            }
            else
            {
                this.albumTableBindingSource.DataSource = this.bandTableBindingSource;
                this.albumTableBindingSource.DataMember = "FK_AlbumTable_BandTable";
            }
        }
    }
}
